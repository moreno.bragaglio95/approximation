# Approximation

### How to compile
To compile approximation project:
```sh
mkdir build  
cd build  
cmake ..  
make  
./approximation
```

### How to test if HIF is correctly installed
Where you want:
```sh
mkdir test  
cd test  
mkdir sources  
cd sources  
touch mult.v  
gedit mult.v
```

Paste the following design:
```verilog
module simple_combinational_mult(product,multiplier,multiplicand);  
    input [15:0]  multiplier, multiplicand;  
    output        product;  
    reg [31:0]    product;  
    integer       i;  
    
    always @( multiplier or multiplicand )  
    begin  
        product = 0;  
        for(i=0; i<16; i=i+1)  
            if( multiplier[i] == 1'b1 ) product = product + ( multiplicand << i );  
    end  
endmodule
```

Then:
```sh
verilog2hif mult.v --output design  
```
In this way a design.hif.xml will be generated.   
It is the rapresentation of the Verilog design through XML syntax.



