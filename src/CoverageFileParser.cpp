#include "CoverageFileParser.hpp"


// Constructor
CoverageFileParser::CoverageFileParser(std::string projDirPath,
                                       std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap,
                                       std::unordered_map<std::string, std::unordered_map<size_t,
                                               std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap) {

    this->projDirPath = projDirPath;
    this->statementCoverageMap = statementCoverageMap;
    this->branchCoverageMap = branchCoverageMap;
}


// This code parses a file containing information about statement coverage.
// This program assumes that in the txt coverage file, the word "File" is used ONLY as in the next structure example !!!
// Also assumes that the structure of the statement coverage file is exactly the same of the next structure example.
// The coverage file MUST contain zero or more instances of the following (example) structure:
//
//                Line         Item                      Count     Source
//                ----         ----                      -----     ------
//             File ALU.v
//                15              1                          1
//                16              1                          1
//                18              1                          1
//                20              1                         10
//                23              1                         72
//                24              1                          1
//                25              1                          8
//                27              1                         13
//                28              1                          3
//                29              1                         69
//                30              1                         17
//                34              1                         72
//                36              1                    ***0***
//                37              1                    ***0***
//                38              1                    ***0***
//                39              1                    ***0***
//                41              1                    ***0***
//                43              1                    ***0***
//                46              1                         72
//                48              1                    ***0***
//                50              1                    ***0***
//             ...

// Fill statement coverage map
void CoverageFileParser::setStatementCoverageMap() {

    std::string line, fileName;

    // Open specific_coverage.txt
    std::ifstream ifs(projDirPath + coverageFileName);
    if (ifs.fail()) {
        std::cout << "#> ERROR! \"specific_coverage.txt\" NOT FOUND!" << std::endl
                  << "#>> Make sure the coverage file is inside .../approximation/tests/*proj_name*/ directory."
                  << std::endl << std::endl;
        std::exit(2);
    }


    // Regex for lines containing "File" followed by a verilog file name
    std::regex regexFile{"File"};

    std::string beginDelimiter = "Statement Coverage for instance";
    int lineNumber, hitCounter;

    // For each line
    while (getline(ifs, line)) {

        // When finds the word "Statement Coverage for instance"
        if (line.find(beginDelimiter) == 0) {

//          specific_coverage.txt example:
//
//  --->    Statement Coverage for instance /vending_machine_test/dut --
//
//              Line         Item                      Count     Source
//              ----         ----                      -----     ------
//  --->      File /home/samu/Desktop/vendingMachine/vending_machine.v
//              68              1                          1
//              ...

            for (int i = 0; i < 4; ++i)
                getline(ifs, line);

            // If NOT finds the word "File" after 4 row
            if (!regex_search(line, regexFile)) {
                continue;
            }


            // Get only the verilog filename, not the word "File"
            std::istringstream iss(line);
            std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                            std::istream_iterator<std::string>{}};
            fileName = tokens[tokens.size() - 1];

            // Only filename, without path
            if (fileName.find('/') != std::string::npos)
                fileName = fileName.substr(fileName.find_last_of("\\/") + 1);

            // Create a new entry in coverage map
            statementCoverageMap->insert({fileName, std::unordered_map<size_t, size_t>()});

            // Save "Line" "Item" and "Count" of the txt coverage file into a vector and fill the statementCoverageMap
            while (getline(ifs, line)) { // Reading of lines after the name of the module
                std::istringstream iss(line);
                std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                                std::istream_iterator<std::string>{}};

//          specific_coverage.txt example:
//
//                  Line         Item                      Count     Source
//                  ----         ----                      -----     ------
//                File sobel.v
//                  24              1                     932737
//                  ...
//                  46              1                     262146
//  [empty] -->
//                ...

                if (tokens.empty())
                    break;


                // Skip header and separator lines
                if (tokens[0].find_first_not_of("0123456789") == std::string::npos) {
                    lineNumber = std::atoi(tokens[0].c_str());
                    hitCounter = std::atoi(tokens[2].c_str());

                    // Save hit counter if greater than saved one (2+ instances)
                    if (statementCoverageMap->operator[](fileName).operator[](lineNumber) < hitCounter)
                        statementCoverageMap->operator[](fileName).operator[](lineNumber) = hitCounter;
                }
            }
        }
    }
}


// This code parses a file containing information about branch coverage.
// This program assumes that the structure of the branch coverage file is exactly the same of the next structure example.
// The coverage file MUST contain zero or more instances of the following (example) structure:
//
// Branch Coverage for instance /vending_machine_test/dut
//
//     Line         Item                      Count     Source
//     ----         ----                      -----     ------
// File vendingMachine/vending_machine.v
// ------------------------------------IF Branch------------------------------------
//     74                                         1     Count coming in to IF
//     74              1                          1
//     78              1                    ***0***
//     80              1                    ***0***
//     82              1                    ***0***
//     84              1                    ***0***
//                                          ***0***     All False Count
// Branch totals: 1 hit of 6 branches = 16.66%
//
// ------------------------------------IF Branch------------------------------------
//     92                                       101     Count coming in to IF
//     92              1                          3
//     94              1                         98
// Branch totals: 2 hits of 2 branches = 100.00%

// Fill branch coverage map
void CoverageFileParser::setBranchCoverageMap() {

    std::string line, fileName;

    // Open specific_coverage.txt
    std::ifstream ifs(projDirPath + "specific_coverage.txt");
    if (ifs.fail()) {
        std::cout << "#> ERROR! \"specific_coverage.txt\" NOT FOUND!" << std::endl
                  << "#>> Make sure the coverage file is inside .../approximation/tests/*proj_name*/ directory."
                  << std::endl << std::endl;
        std::exit(2);
    }


    // Regex for lines containing "File" followed by a verilog file name
    std::regex regexFile{"File"};

    std::string beginDelimiter = "Branch Coverage for instance";
    std::string endDelimiter = "=============";

    // Where to find totalHit
    std::string conditionStartDelimiter = "Count coming in to";
    std::string conditionEndDelimiter = "Branch totals:";

    // Zero value string
    std::string zeroValueString = "***0***";

    // Total condition hit
    size_t totalHit;

    // Remaining condition hit
    // E.g. if() ... else if() ... else if() ...:
    // ------------------------------------IF Branch------------------------------------
    //    92                                       101     Count coming in to IF
    //    92              1                          3     <--[remainingHit = 101 = totalHit]
    //    94              1                         48     <--[remainingHit =  98 = (101 - 3)]
    //    96              1                         40     <--[remainingHit =  40 = (98 - 48)]
    size_t remainingHit;

    // Condition line number, item number, hit counter. E.g.
    //    94              1                         48
    size_t lineNumber, itemNumber, hitCounter;

    hif::ClassId classId;


    // For each line
    while (getline(ifs, line)) {

        // When finds the word "Statement Coverage for instance"
        if (line.find(beginDelimiter) == 0) {

            // Skip 3 line
            for (int i = 0; i < 4; ++i)
                getline(ifs, line);

            // If NOT finds the word "File" after 4 line
            if (!regex_search(line, regexFile)) {
                continue;
            }


            // Get only the verilog filename, not the word "File"
            std::istringstream iss(line);
            std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                            std::istream_iterator<std::string>{}};
            fileName = tokens[tokens.size() - 1];

            // Only filename, without path
            if (fileName.find('/') != std::string::npos)
                fileName = fileName.substr(fileName.find_last_of("\\/") + 1);

            // Create a new entry in coverage map
            branchCoverageMap->insert(
                    {fileName, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >()});


            do {
                getline(ifs, line);

                // Starting coverage line, e.g.:
                //     92                                       101     Count coming in to IF
                if (line.find(conditionStartDelimiter) == std::string::npos)
                    continue;


                // Save "Line" "Total condition hit" and "Condition type"
                //    105                                       21     Count coming in to IF
                std::istringstream iss(line);
                std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                                std::istream_iterator<std::string>{}};

                // Condition hit (total: true hit + false hit)
                if (tokens[1] == zeroValueString)
                    totalHit = 0;
                else
                    totalHit = std::stoi(tokens[1]);
                remainingHit = totalHit;

                classId = getClassIdFromCoverageString(tokens[6]);


                // Save "Line" "Item" and "Count" of the txt coverage file into a vector and fill the branchCoverageMap
                while (getline(ifs, line) && line.find(conditionEndDelimiter) ==
                                             std::string::npos) { // Reading of lines after the name of the module
                    std::istringstream iss(line);
                    std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                                    std::istream_iterator<std::string>{}};

                    // Skip header and separator lines
                    if (tokens[0].find_first_not_of("0123456789*") == std::string::npos) {

                        // Skip "all false" counter, e.g.:
                        //                                               2     All False Count
                        if (tokens.size() == 3) {

                            // Line example:
                            //     169             1                          2
                            lineNumber = std::atoi(tokens[0].c_str());
                            itemNumber = std::atoi(tokens[1].c_str());

                            // If "***0***" --> 0
                            if (tokens[2] == zeroValueString)
                                hitCounter = 0;
                            else
                                hitCounter = std::atoi(tokens[2].c_str());


                            // Save remainingHit if greater than saved one (2+ instances)
                            if (std::get<0>(branchCoverageMap->operator[](fileName).operator[](lineNumber)) <
                                remainingHit) {

                                branchCoverageMap->operator[](fileName).operator[](lineNumber) = {remainingHit,
                                                                                                  totalHit,
                                                                                                  itemNumber,
                                                                                                  classId};
                            }


                            // remainingHit: condition hit (true hit + false hit)
                            remainingHit = remainingHit - hitCounter;
                        }
                    }
                }

            } while (line.find(endDelimiter) != 0);
        }
    }
}


// Given a coverage condition string (e.g. "IF", "CASE"), returns the hif::ClassId.
//
// Converage file has two types of condition: "IF" (If, Ifelse, Ternary) and "CASE" (Switch-case).
// While and For are in statement coverage, not in branch coverage.
hif::ClassId CoverageFileParser::getClassIdFromCoverageString(std::string classString) {

    if (classString == "IF") {
        return hif::CLASSID_IF;
    } else if (classString == "CASE") {
        return hif::CLASSID_SWITCH;
    }

    // If IFs are false than it's an unexpected case, not handled.
    std::cout << std::endl << "ERROR DURING COVERAGE PARSING!" << std::endl
              << classString << " not handled!" << std::endl;
    std::exit(1);
}
