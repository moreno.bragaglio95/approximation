#include "ast2Bmatrix.hpp"


// Constructor
ast2Bmatrix::ast2Bmatrix(Bmatrix *bmatrix,
                         Graph *graph,
                         std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap,
                         std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap,
                         std::vector<std::map<std::string, size_t>> *assertionDepths,
                         std::vector<std::pair<size_t, size_t>> *assertionContingency,
                         std::string projDirPath) {

    this->bmatrix = bmatrix;
    this->graph = graph;
    this->statementCoverageMap = statementCoverageMap;
    this->branchCoverageMap = branchCoverageMap;
    this->assertionDepths = assertionDepths;
    this->assertionContingency = assertionContingency;
    this->projDirPath = projDirPath;

    verbose = isVerbose();
}


// <LIBRARYDEFS> visitors
int ast2Bmatrix::visitLibraryDef(hif::LibraryDef &o) {
    // For the LIBRARYDEFS xml tag there will not be visiting. Pass directly to the next tag
    return 0;
}


// <IDENTIFIER> visitors:
//
// If it's inside the LSH of an ASSIGN
//     add the identifier deps to the assign deps
// If it's inside CONDITION
//     add the minimum deps between id deps and top condition deps
//     (e.g. "a < b", save minimum deps between "a" and "b")
// If it's inside SIGNAL and VALUE (signal declaration with initialization)
//     get minimum deps between id deps and signal deps
//
// Note: "dep" = "dependencies"
int ast2Bmatrix::visitIdentifier(hif::Identifier &o) {

    // If it's inside an assign and it's in LHS
    if (isLeftOrRightHandSide(&o) == 'l') {
        std::vector<dependency> objDeps = getDeps(&o);

        // Get minimum distances. E.g. {a,b} = 2'b01 --> min(dep(a), dep(b))
        getMinDependencies(&assignDeps, &objDeps);
    }

    // If it's inside a condition
    if (isInsideTagName(&o, conditionTagName)) {
        std::vector<dependency> objDeps = getDeps(&o);

        // Get minimum distances. E.g. if(a > b) --> min(dep(a), dep(b))
        getMinDependencies(&conditionDeps.top().second, &objDeps);
    }

    // Mute it if you want to skip signal declarations WITH initialization (values not considered)
    if (isInsideClassTag(&o, hif::CLASSID_SIGNAL) && isInsideTagName(&o, valueTagName)) {

        // First Identifier that visit the Signal initialization
        if (lastVisitedSignal != nullptr) {
            std::vector<dependency> objDeps = getDeps(lastVisitedSignal);

            bmatrix->add(lastVisitedSignal, lastVisitedSignal->getSourceLineNumber(),
                         getLine(&o, true), objDeps,
                         getCoverage(lastVisitedSignal), getObjImpact(lastVisitedSignal));

            lastVisitedSignal = nullptr;
        }
    }

    return hif::GuideVisitor::visitIdentifier(o);
}


// <SIGNAL> visitors
int ast2Bmatrix::visitSignal(hif::Signal &o) {
    lastVisitedSignal = &o;

    return hif::GuideVisitor::visitSignal(o);
}


// Generic visitor, BEFORE visit
bool ast2Bmatrix::BeforeVisit(hif::Object &o) {

    // Conditions
    if (isCondition(&o)) {

        // <current object*, empty dependency>
        conditionDeps.push({&o, std::vector<dependency>()});
    }

    return HifVisitor::BeforeVisit(o);
}


// Generic visitor, AFTER visit:
//
// If it's an ASSIGN
//     set genLineNumber and genSourceFile *
//     add the ASSIGN to the B matrix
// If it's a CONDITION
//     set genLineNumber and genSourceFile **
//     add the CONDITION to the B matrix
// If it's a SIGNAL
//     reset lastVisitedSignal
//
// Note*: INITVALUES and STEPACTION in a FORGENERATE doens't have CODE_INFO tag
//        sourceFileName and sourceLineNumber can be found visiting LHS or RHS identifiers
// Note**: IFGENERATE and FORGENERATE doesn't have CODE_INFO tag
//         sourceFileName and sourceLineNumber can be found visiting condition identifiers
int ast2Bmatrix::AfterVisit(hif::Object &o) {

    // If visited object is an ASSIGN
    if (dynamic_cast<hif::Assign *>(&o) != nullptr) {

        // Get line number and filename
        genLineNumber = o.getSourceLineNumber();
        genSourceFile = o.getSourceFileName();


        // <ASSIGN> doesn't have <CODE_INFO> if it's inside <INITVALUES> or <STEPACTIONS> of a <FORGENERATE>
        if (genLineNumber == 0) {
            hif::Assign *asgn = dynamic_cast<hif::Assign *>(&o);

            // Visits the LHS if LHS is not empty
            if (asgn->getLeftHandSide()->getSourceLineNumber() != 0) {
                // Get line number and filename (LHS)
                genLineNumber = asgn->getLeftHandSide()->getSourceLineNumber();
                genSourceFile = asgn->getLeftHandSide()->getSourceFileName();

            } else {    // Visits the RHS
                // Get line number and filename (RHS)
                genLineNumber = asgn->getRightHandSide()->getSourceLineNumber();
                genSourceFile = asgn->getRightHandSide()->getSourceFileName();
            }
        }

        // Add Assign to B matrix
        bmatrix->add(&o, genLineNumber, getLine(&o, true), assignDeps,
                     getCoverage(&o), getImpact(assignDeps));

        assignDeps.clear();
        genLineNumber = 0;
        genSourceFile = "";


    } else if (isCondition(&o)) {   // If visited object is a condition (IF, SWITCH-CASE, ...)

        // "initial" has condition tag
        if (conditionDeps.top().second.size() != 0) {

            // Get line number and filename
            genLineNumber = o.getSourceLineNumber();
            genSourceFile = o.getSourceFileName();


            // <IFGENERATE> doesn't have <CODE_INFO> then look in <CONDITION>
            hif::IfGenerate *ifgen = dynamic_cast<hif::IfGenerate *>(&o);
            if (ifgen != nullptr) {
                genLineNumber = ifgen->getCondition()->getSourceLineNumber();
                genSourceFile = ifgen->getCondition()->getSourceFileName();
            }

            // <FORGENERATE> doesn't have <CODE_INFO> then look in <CONDITION>
            hif::ForGenerate *forgen = dynamic_cast<hif::ForGenerate *>(&o);
            if (forgen != nullptr) {
                genLineNumber = forgen->getCondition()->getSourceLineNumber();
                genSourceFile = forgen->getCondition()->getSourceFileName();
            }


            // Add condition to B matrix
            bmatrix->add(&o, genLineNumber, getLine(&o, true),
                         conditionDeps.top().second, getCoverage(&o),
                         getImpact(conditionDeps.top().second));


            genLineNumber = 0;
            genSourceFile = "";
        }

        conditionDeps.pop();


    } else if (dynamic_cast<hif::Signal *>(&o) != nullptr) {    // If visited object is a SIGNAL
        lastVisitedSignal = nullptr;
    }

    return HifVisitor::AfterVisit(o);
}


// Given an object, return the dependency vector
std::vector<dependency> ast2Bmatrix::getDeps(hif::Object *obj) {
    // B matrix outputs
    std::vector<hif::Object *> *outputs = bmatrix->getColumns();

    // Calculate minimum distances
    std::vector<dependency> dependencies;

    for (int i = 0; i < outputs->size(); ++i)
        dependencies.push_back(graph->minimumDistance(obj, outputs->operator[](i)));

    return dependencies;
}


// Given an object, return the impact
float ast2Bmatrix::getObjImpact(hif::Object *obj) {
    // B matrix outputs
    std::vector<hif::Object *> *outputs = bmatrix->getColumns();

    // Calculate minimum distances
    std::vector<dependency> deps = getDeps(obj);

    return getImpact(deps);
}



// Given two dependencies vector (depsA and depsB), save the minimum dependencies vector in depsA.
// Usage: when there are more than one object in a condition (e.g. "a > b"), minimum distances are saved.
void ast2Bmatrix::getMinDependencies(std::vector<dependency> *depsA, std::vector<dependency> *depsB) {

    // If depsA not empty, get minimum values
    if (!depsA->empty()) {

        // For each dependency
        for (int i = 0; i < depsA->size(); ++i) {

            // If I-th output depends in depsB, not in depsA
            if (!depsA->operator[](i).first && depsB->operator[](i).first) {

                depsA->operator[](i).first = true;
                depsA->operator[](i).second = depsB->operator[](i).second;

            } else if (depsA->operator[](i).first && depsB->operator[](i).first) {
                // I-th output depends in depsB and in depsA

                // Get minimum
                if (depsB->operator[](i).second < depsA->operator[](i).second)
                    depsA->operator[](i).second = depsB->operator[](i).second;
            }
        }
    } else {
        *depsA = *depsB;
    }
}


// Get the code line of obj
//
// If <FORGENERATE> or <IFGENERATE>, you have to use genSourceFile and genLineNumber
// Fix filename: removing the path and adding project directory path (sourceFileName may have partial/different path)
// Read file, looking for the n-th line (n = lineNumber or genLineNumber)
//      If line not found, raise error
// Open source file and return the n-th line (n = obj line number).
std::string ast2Bmatrix::getLine(hif::Object *obj, bool trimIt) {

    std::string verilogCodeDir = "verilog/";

    if (verbose) {
        std::cout << "ROW: " << obj->getSourceLineNumber() << ", COL " << obj->getSourceColumnNumber()
                  << ", CLASS: " << getClassIDString(obj) << ", DU: " << getObjName(getDesignUnit(obj)) << std::endl
                  << "FILE:" << obj->getSourceFileName() << std::endl
                  << "-" << std::endl;  // Separator
    }

    // Get filename, it may contain (a piece of) the path
    std::string fileVerilog = obj->getSourceFileName();

    // Obj line number
    int lineNumber = obj->getSourceLineNumber();

    // If <FORGENERATE> or <IFGENERATE>
    if (genLineNumber != 0) {
        fileVerilog = genSourceFile;
        lineNumber = genLineNumber;
    }


    // This exception occurs when an examined Hif object doesn't have sourceFileName
    if (fileVerilog == "") {
        std::cout << std::endl << "#> UNEXPECTED ERROR! SOURCE FILE NOT FOUND!" << std::endl << std::endl;
        std::exit(1);
    }


    // Removing path from filename
    if (fileVerilog.find('/') != std::string::npos) {
        // "/home/.../file.v" --> "file.v"
        fileVerilog = fileVerilog.substr(fileVerilog.find_last_of('/') + 1);
    }

    // Add project directory path (e.g. .../approximation/tests/*myProjName*/verilog/) to filename
    std::string fileVerilogWithPath = projDirPath + verilogCodeDir + fileVerilog;


    std::string line;
    int i;
    std::ifstream ifs(fileVerilogWithPath);

    // Reading the file
    for (i = 1; getline(ifs, line); ++i) {
        // If the desired line is found, returns it
        if (i == lineNumber) {
            if (trimIt)
                return trim(line);
            else
                return line;
        }
    }


    // Errors
    if (i == 1) {    // No lines read  --> not found or empty file
        std::cout << std::endl << "#> ERROR! FILE NOT FOUND OR EMPTY!" << std::endl
                  << "#>> \"" << fileVerilog
                  << "\" not found inside .../approximation/tests/*projName*/verilog/ directory or it's empty."
                  << std::endl << std::endl;

    } else {    // Lines read --> Hif.xml file or Verilog file modified
        std::cout << std::endl << "#> ERROR DURING FILE READ!" << std::endl
                  << "#>> Make sure \"" << fileVerilog
                  << "\" is the same used in verilog2hif command. Please recreate hif.xml file."
                  << std::endl << std::endl;
    }

    std::exit(1);
}


// Get the coverage hit of obj
//
// If <FORGENERATE> or <IFGENERATE>, you have to use genSourceFile and genLineNumber
// Get the source file name of obj without path
// Get the coverage hit:
//      if obj is IF or CASE, look in branchCoverageMap
//      otherwise, look in statementCoverageMap
//
// statementCoverageMap: assign, for condition, while condition
// branchCoverageMap: switch condition, if condition.
size_t ast2Bmatrix::getCoverage(hif::Object *obj) {

    std::string filename = obj->getSourceFileName();
    int lineNumber = obj->getSourceLineNumber();

    // If <FORGENERATE> or <IFGENERATE>
    if (genLineNumber != 0 || !genSourceFile.empty()) {
        filename = genSourceFile;
        lineNumber = genLineNumber;
    }

    // If filename still empty --> error
    if (filename.empty()) {
        std::cout << "#> <FORGENERATE>/<IFGENERATE> ERROR!" << std::endl;
        std::exit(1);
    }

    // Removing path from filename
    if (filename.find('/') != std::string::npos) {
        // "/home/.../file.v" --> "file.v"
        filename = filename.substr(filename.find_last_of("\\/") + 1);
    }


    try {
        // Get coverage hit
        // If assign, for condition or while condition --> statementCoverageMap
        if (!isCondition(obj)
            || dynamic_cast<hif::While *>(obj) != nullptr && !dynamic_cast<hif::While *>(obj)->isDoWhile()
            || dynamic_cast<hif::For *>(obj) != nullptr)
            return statementCoverageMap->operator[](filename).at(lineNumber);
        else    // If switch condition or if condition --> branchCoverageMap
            return std::get<0>(branchCoverageMap->operator[](filename).at(lineNumber));


    } catch (const std::out_of_range &e) {
        // Out_of_range exception --> the line isn't in coverage file
        return -1;
    }
}


// Given a dependencies vector, return the impact (of the statement) on the assertion set
float ast2Bmatrix::getImpact(std::vector<dependency> deps) {

    if (assertionDepths->empty()) {
        return -1;
    }


    std::vector < hif::Object * > outputs = bmatrix->getOutputs();
    float sum = 0;
    int counter = 0, j = 0;

    for (auto assertionMap : *assertionDepths) {
        for (int i = 0; i < outputs.size(); ++i) { // For each output of the B matrix

            // If output inside assertion consequent
            if (assertionMap.find(getObjName(outputs[i])) != assertionMap.end()) {

                if (deps[i].first) { // If output affected
                    if (assertionContingency->empty()) { // If assertion contingency file not found
                        // 1 / [1+(depth*distance)]
                        sum += 1.0 / (1.0 + (assertionMap[getObjName(outputs[i])] * deps[i].second));
                    } else {
                        // (1 / [1+(depth*distance)])*(TrueAntTrueCons istances/All istances)
                        sum += (1.0 / (1.0 + (assertionMap[getObjName(outputs[i])] * deps[i].second)))
                               * (assertionContingency->operator[](j).second /
                                  (1.0 * assertionContingency->operator[](j).first));
                    }

                }
                ++counter;
            }
        }
        ++j;
    }

    return sum / counter;
}
