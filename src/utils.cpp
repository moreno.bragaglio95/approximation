#include "utils.hpp"


// If modifyVerbose = True, set verbose
// Retun True if if verbose output is enabled
bool setVerbose(bool modifyVerbose, bool isVerbose) {
    static bool verbose;

    if (modifyVerbose)
        verbose = isVerbose;

    return verbose;
}


// Return True if verbose output is enabled
bool isVerbose() {
    return setVerbose(false, false);
}


// Return True if they are the same object (same name and same design unit).
bool areSameObject(hif::Object *nodeA, hif::Object *nodeB) {

    if (getObjName(nodeA) == getObjName(nodeB) &&
        getDesignUnit(nodeA) == getDesignUnit(nodeB)) {

        return true;
    }

    return false;
}


// Given an object, visit the HIF tree backwards. If the Design unit pointer is found, return it.
hif::DesignUnit *getDesignUnit(hif::Object *obj) {
    hif::Object *du = obj;

    // Looking for <DESIGNUNIT>
    while (du != nullptr && dynamic_cast<hif::DesignUnit *>(du) == nullptr)
        du = du->getParent();

    return dynamic_cast<hif::DesignUnit *>(du);
}


// Given an object, visit the HIF tree backwards. Return True if a classId object is found.
bool isInsideClassTag(hif::Object *o, hif::ClassId classId) {

    do {
        // Looking for classId
        if (hif::classIDToString(o->getClassId()) == hif::classIDToString(classId))
            return true;

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return false;
}


// Given an object, visit the HIF tree backwards. Return the classId of the nearest classId1 or classId2 object.
hif::ClassId nearestClassTag(hif::Object *o, hif::ClassId classId1, hif::ClassId classId2) {

    do {
        // Looking for classId1 or classId2
        if (hif::classIDToString(o->getClassId()) == hif::classIDToString(classId1))
            return classId1;
        else if (hif::classIDToString(o->getClassId()) == hif::classIDToString(classId2))
            return classId2;

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return hif::CLASSID_NULL;
}


// Given an object, visit the HIF tree backwards. If a classId object is found, return it.
hif::Object *returnClassObj(hif::Object *o, hif::ClassId classId) {

    do {
        // Looking for an Object* of classId
        if (hif::classIDToString(o->getClassId()) == hif::classIDToString(classId))
            return o;

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return nullptr;
}


// Given an object, visit the HIF tree backwards. Return True if a object with tagName is found.
//
// Note: Some xml tag doesn't have its own HIF class. In this case, you need to search the HIF tag name.
//          E.g. <CONDITION>, tagName = "condition"
//       Some xml tag doesn't have its own HIF class and HIF tag name.
//          E.g. <INITVALUES>, <STEPACTIONS>
bool isInsideTagName(hif::Object *o, std::string tagName) {

    do {
        // Looking for tagName
        if (strcmp(o->getFieldName().c_str(), tagName.c_str()) == 0)
            return true;

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return false;
}


// Given an object, visit the HIF tree backwards. Return the tagName of the nearest tagName1 or tagName2 object.
//
// Note: Some xml tag doesn't have its own HIF class. In this case, you need to search the HIF tag name.
//          E.g. <CONDITION>, tagName = "condition"
//       Some xml tag doesn't have its own HIF class and HIF tag name.
//          E.g. <INITVALUES>, <STEPACTIONS>
std::string nearestTagName(hif::Object *o, std::string tagName1, std::string tagName2) {

    do {
        // Looking for tagName1 or tagName2
        if (strcmp(o->getFieldName().c_str(), tagName1.c_str()) == 0)
            return tagName1;
        else if (strcmp(o->getFieldName().c_str(), tagName2.c_str()) == 0)
            return tagName2;

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return "";
}


// Given an object, visit the HIF tree backwards. Return the string of the nearest classId or tagName object.
std::string nearestClassOrTagName(hif::Object *o, hif::ClassId classId, std::string tagName) {

    do {
        // Looking for classId or tagName
        if (hif::classIDToString(o->getClassId()) == hif::classIDToString(classId))
            return hif::classIDToString(classId);
        else if (strcmp(o->getFieldName().c_str(), tagName.c_str()) == 0)
            return tagName;

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return "";
}


// Return 'l' or 'r' if the object is inside the left hand side or the right hand side of the assign.
// If not inside an assign, return ' '
char isLeftOrRightHandSide(hif::Object *o) {

    do {
        // Looking for RHS tag or LHS tag
        if (strcmp(o->getFieldName().c_str(), rhsTagName.c_str()) == 0)
            return 'r';
        else if (strcmp(o->getFieldName().c_str(), lhsTagName.c_str()) == 0)
            return 'l';

        o = o->getParent();

    } while (o->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return ' ';
}


// Return True if the object is inside an index
//
// E.g. "i" in "a[i]" (the object is inside <INDEX>)
//      "i" in "a[j:i]" (the object is inside <RANGE>)
bool isIndex(hif::Object *o) {
    // If inside <INDEX> or <RANGE>
    if (isInsideTagName(o, indexTagName) || isInsideClassTag(o, hif::CLASSID_RANGE))
        return true;

    return false;
}


// Return True if the object is a condition
bool isCondition(hif::Object *obj) {

    if ((dynamic_cast<hif::If *>(obj) != nullptr) ||            // <IF>
        (dynamic_cast<hif::IfGenerate *>(obj) != nullptr) ||    // <IFGENERATE>
        (dynamic_cast<hif::Switch *>(obj) != nullptr) ||        // <SWITCH>
        (dynamic_cast<hif::For *>(obj) != nullptr) ||           // <FOR>
        (dynamic_cast<hif::ForGenerate *>(obj) != nullptr) ||   // <FORGENERATE>
        (dynamic_cast<hif::StateTable *>(obj) != nullptr) ||    // <STATETABLE>
        ((dynamic_cast<hif::When *>(obj) != nullptr &&          // <WHEN>:
          dynamic_cast<hif::When *>(obj)->isLogicTernary())) || // isLogicTernary()=True, it's Ternary
        ((dynamic_cast<hif::While *>(obj) != nullptr) &&        // <WHILE>: (isDoWhile()=True, "a<=b")
         !dynamic_cast<hif::While *>(obj)->isDoWhile())) {      // isDoWhile()=False, while loop

        return true;
    }

    return false;
}


// Returns True if the object is a value
bool isValue(hif::Object *obj) {

    if (dynamic_cast<hif::BitValue *>(obj) != nullptr ||
        dynamic_cast<hif::BitvectorValue *>(obj) != nullptr ||
        dynamic_cast<hif::BoolValue *>(obj) != nullptr ||
        dynamic_cast<hif::CharValue *>(obj) != nullptr ||
        dynamic_cast<hif::EnumValue *>(obj) != nullptr ||
        dynamic_cast<hif::IntValue *>(obj) != nullptr ||
        dynamic_cast<hif::RealValue *>(obj) != nullptr ||
        dynamic_cast<hif::StringValue *>(obj) != nullptr ||
        dynamic_cast<hif::TimeValue *>(obj) != nullptr ||
        dynamic_cast<hif::ConstValue *>(obj) != nullptr ||
        dynamic_cast<hif::RecordValueAlt *>(obj) != nullptr ||
        dynamic_cast<hif::RecordValue *>(obj) != nullptr) {

        return true;
    }

    return false;
}


// Given an object, return the name
std::string getObjName(hif::Object *obj) {

    if (dynamic_cast<hif::Identifier *>(obj) != nullptr)                     // <IDENTIFIER>
        return dynamic_cast<hif::Identifier *>(obj)->getName()->c_str();

    else if (dynamic_cast<hif::DesignUnit *>(obj) != nullptr)                // <DESIGNUNIT>
        return dynamic_cast<hif::DesignUnit *>(obj)->getName()->c_str();

    else if (dynamic_cast<hif::Port *>(obj) != nullptr)                      // <PORT>
        return dynamic_cast<hif::Port *>(obj)->getName()->c_str();

    else if (dynamic_cast<hif::PortAssign *>(obj) != nullptr)                // <PORTASSIGN>
        return dynamic_cast<hif::PortAssign *>(obj)->getName()->c_str();

    else if (dynamic_cast<hif::Signal *>(obj) != nullptr)                    // <SIGNAL>
        return dynamic_cast<hif::Signal *>(obj)->getName()->c_str();

    else if (dynamic_cast<hif::ValueTP *>(obj) != nullptr)                   // <VALUETP>
        return dynamic_cast<hif::ValueTP *>(obj)->getName()->c_str();

    return "";
}


// Return the value of the object as a string
std::string getValueAsString(hif::Object *obj) {
    std::ostringstream strs;

    if (dynamic_cast<hif::BitValue *>(obj) != nullptr) {
        strs << bitConstantToString(dynamic_cast<hif::BitValue *>(obj)->getValue());

    } else if (dynamic_cast<hif::BitvectorValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::BitvectorValue *>(obj)->getValue().c_str();

    } else if (dynamic_cast<hif::BoolValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::BoolValue *>(obj)->getValue();

    } else if (dynamic_cast<hif::CharValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::CharValue *>(obj)->getValue();

    } else if (dynamic_cast<hif::EnumValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::EnumValue *>(obj)->getValue();

    } else if (dynamic_cast<hif::IntValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::IntValue *>(obj)->getValue();

    } else if (dynamic_cast<hif::RealValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::RealValue *>(obj)->getValue();

    } else if (dynamic_cast<hif::StringValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::StringValue *>(obj)->getValue();

    } else if (dynamic_cast<hif::TimeValue *>(obj) != nullptr) {
        strs << dynamic_cast<hif::TimeValue *>(obj)->getValue();

    } else {
        strs << " ";
    }
    // More cases? ConstValue, RecordValueAlt, RecordValue?

    return strs.str();
}


// Return the value or the name of the object
std::string getObjString(hif::Object *obj) {

    if (isValue(obj))
        return getValueAsString(obj);
    else
        return getObjName(obj);
}


// Return the classId if the object as a string (removed "Alt", "Generate" and added "Ternary")
std::string getClassIDString(hif::Object *obj) {

    hif::ClassId id = obj->getClassId();

    switch (id) {
        // Fix strings (E.g. "For" instead "ForGenerate")
        case hif::CLASSID_FORGENERATE:
            id = hif::CLASSID_FOR;
            break;
        case hif::CLASSID_IFALT:
            id = hif::CLASSID_IF;
            break;
        case hif::CLASSID_IFGENERATE:
            id = hif::CLASSID_IF;
            break;
        case hif::CLASSID_RECORDVALUEALT:
            id = hif::CLASSID_RECORDVALUE;
            break;
        case hif::CLASSID_SWITCHALT:
            id = hif::CLASSID_SWITCH;
            break;
        case hif::CLASSID_WHENALT:
            id = hif::CLASSID_WHEN;
            break;
        case hif::CLASSID_WITHALT:
            id = hif::CLASSID_WITH;
            break;
        case hif::CLASSID_WHEN: // Ternary operator
            if (dynamic_cast<hif::When *>(obj)->isLogicTernary())
                return "Ternary";

        default:;
    }

    return hif::classIDToString(id);
}


// Return True if the object is inside the vector
bool isInsideVector(hif::Object *obj, std::vector<hif::Object *> *objVector) {
    int i;

    // For each object in the objVector
    for (i = 0; i < objVector->size(); ++i) {

        // If same name and same design unit
        if (areSameObject(objVector->operator[](i), obj))
            return true;
    }

    return false;
}


// Given an object, visit the HIF tree backwards. Returns True if the object is inside a condition.
// True if:
//      inside sensitivity list
//      or inside CONDITION
//      or inside FORGENERATE and not inside GLOBALACTION
bool isPartOfCondition(hif::Object *o) {

    hif::Object *obj = o;

    bool inForGenTag = false;
    bool inGlobActTag = false;
    bool inCond = false;


    // If in sensitivity list
    if (obj->getParent()->getClassId() == hif::CLASSID_STATETABLE)
        return true;

    // Generic workflow
    do {
        // If obj is in <CONDITION>
        // id in <FOR> not recognized! Because For has 3 parts (initvals; conditions; stepactions)
        if (strcmp(obj->getFieldName().c_str(), conditionTagName.c_str()) == 0) {
            inCond = true;
        }

        // If obj is in a ForGenerate loop but not in its body
        if (obj->getClassId() == hif::CLASSID_FORGENERATE) {
            inForGenTag = true;
        }

        // If obj is in <GLOBALACTION>
        if (obj->getClassId() == hif::CLASSID_GLOBALACTION) {
            inGlobActTag = true;
        }

        // Recursive call
        if (obj->getParent()->getClassId() != hif::CLASSID_SYSTEM)
            obj = obj->getParent();

    } while (obj->getParent()->getClassId() != hif::CLASSID_SYSTEM);   // <SYSTEM> is the root tag

    return inCond || (inForGenTag && !inGlobActTag);
}


// Assign ofstream to cout. It returns the "backup" of the current cout.
std::streambuf *setCout(std::ofstream *file) {
    std::streambuf *psbuf, *backup;

    // Redirect std::cout to file
    backup = std::cout.rdbuf();     // Back up cout's streambuf
    psbuf = file->rdbuf();          // Get file's streambuf
    std::cout.rdbuf(psbuf);         // Assign streambuf to cout

    return backup;
}


// Assign streambuf to cout
void setCout(std::streambuf *sb) {
    std::cout.rdbuf(sb);
}


// Left trim
std::string ltrim(const std::string &s) {
    return std::regex_replace(s, std::regex("^\\s+"), std::string(""));
}


// Right trim
std::string rtrim(const std::string &s) {
    return std::regex_replace(s, std::regex("\\s+$"), std::string(""));
}


// Left and right trim
std::string trim(const std::string &s) {
    return ltrim(rtrim(s));
}
