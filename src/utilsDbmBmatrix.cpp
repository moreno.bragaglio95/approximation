#include "utilsDbmBmatrix.hpp"
#include <ast2Bmatrix.hpp>


// After order mode selection, it creates a B matrix for each Design unit in duVector
//
// Inputs:
//    - *dbmMap, Data Between Modules map (contains DUs info)
//    - duVector, design unit vector (a B matrix will be created for each design unit of duVector)
//    - *system
//    - *statementCoverageMap, statement coverage map:
//          unord_map< filename, unord_map< line number, # statement hit > >
//    - *branchCoverageMap, branch coverage map:
//          unord_map< filename, unord_map< line number, tuple(
//                                                          # condition hit (only a specific condition),
//                                                          # total condition hit (whole condition block),
//                                                          item number (=n, n-th condition of the line),
//                                                          classId (it specifies the class id: "if" or "switch-case")
//                                                          ) > >
//    - assertionDepths, assertions depth vector:
//          vector< map< identifier string, identifier depth> >
//    - assertionContingency, assertion contingency vector
//          vector< pair{all instances, True antecedent-True consequent instances} >
//    - testDirPath, path of the Test directory (e.g. ".../approximation/tests/*myProjName*/output/test_027/")
//    - projDirPath, path of the Project directory (e.g. ".../approximation/tests/*myProjName*/")
void getBmatrixs(std::map<hif::DesignUnit *, DBM> *dbmMap,
                 std::vector<hif::DesignUnit *> duVector,
                 hif::System *system,
                 std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap,
                 std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap,
                 std::vector<std::map<std::string, size_t>> *assertionDepths,
                 std::vector<std::pair<size_t, size_t>> *assertionContingency,
                 std::string testDirPath,
                 std::string projDirPath) {

    int orderMode;
    int counter = 0;

    // Unordered B matrix for each design unit in duVector
    std::vector<std::vector<bmTuple>> unorderedBmatrixVectors;

    // Port vector for each design unit in duVector
    std::vector<std::vector<hif::Object *>> ports;


    auto timeBegin = std::chrono::high_resolution_clock::now();

    // For each design unit in duVector
    for (int i = 0; i < duVector.size(); ++i) {
        // Get output ports
        ports.push_back(getDuPorts(duVector[i], false, true, dbmMap));

        // Create Bmatrix
        Bmatrix bmatrix(ports[i], orderMode, testDirPath, duVector[i],
                        add3DigitNumberToString("", counter));

        // Fill Bmatrix
        ast2Bmatrix visitorBmatrix(&bmatrix, &(dbmMap->operator[](duVector[i]).cdfg),
                                   statementCoverageMap, branchCoverageMap,
                                   assertionDepths, assertionContingency, projDirPath);
        visitorBmatrix.visitSystem(*system);

        // Save the unordered B matrix of the current design unit
        unorderedBmatrixVectors.push_back(bmatrix.getUnorderedBmatrix());
    }

    auto timeEnd = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(timeEnd - timeBegin).count();
    std::cout << std::endl << " ==== [UNORDERED B MATRIX GENERATED, EXECUTION TIME: " << duration << "ms] ==== "
              << std::endl << std::endl;


    // Until '0' is chosen
    do {
        // Order mode
        orderMode = selectOrderMode();
        ++counter;

        if (orderMode >= 1 && orderMode <= 4) {

            // For each Design Unit
            for (int i = 0; i < duVector.size(); ++i) {

                // Create Bmatrix
                Bmatrix bmatrix(ports[i], orderMode, testDirPath, duVector[i],
                                add3DigitNumberToString("", counter),
                                unorderedBmatrixVectors.operator[](i));
                // Print Bmatrix
                bmatrix.printBmatrix(duVector[i]);

            }

            std::cout << std::endl << " ==== [B MATRIX GENERATED] ==== " << std::endl << std::endl;
        }

    } while (orderMode != 0);
}


// Return the selected order mode
int selectOrderMode() {
    std::string cinStr;
    bool valid = false;

    while (!valid) {
        valid = true;

        // Menu
        std::cout << std::endl << std::endl
                  << "Choose a sorting method:" << std::endl
                  << "\t1. Influenced outputs number" << std::endl
                  << "\t2. Distances average" << std::endl
                  << "\t3. Distances weighted average" << std::endl
                  << "\t4. Outputs order" << std::endl
                  << "\t0. --Exit--" << std::endl
                  << "> ";

        // Read input
        getline(std::cin, cinStr);

        // If unexpected input
        if (cinStr.size() != 1 || (cinStr[0] != '0' && cinStr[0] != '1' &&
                                   cinStr[0] != '2' && cinStr[0] != '3' && cinStr[0] != '4')) {

            std::cout << "Please enter an Integer only (0 <= n <= 4)." << std::endl;
            valid = false;
        }
    }

    return (cinStr[0] - '0');
}


// Remove filename from path (if path doesn't have '/', return "./")
// E.g. ".../*myProjName*/myFile.hif.xml" --> ".../*myProjName*/"
// E.g. "myFile.hif.xml" --> "./"
std::string removeFilenameFromPath(std::string path) {

    if (path.find_last_of("/") != std::string::npos)
        return path.substr(0, path.find_last_of("\\/")) + "/";
    else    // No '/' found
        return "./";
}


// Return the path of the new test directory:
//      - open "output/" (if doesn't exist, create it)
//      - read directory names inside "output/", looking for the highest counter (e.g. 27 in "test_027")
//      - create the test path string (e.g. "..../output" --> "..../output/test_027/")
//      - create the test directory
// E.g. If in the output directory there are "test_003/", "test_020/" and "randomDir_089/"
//      "test_021" will be created
//
// Input: hif file with path (e.g. ".../approximation/tests/*myProjName*/myFile.hif.xml")
// Output: test directory path (e.g. ".../approximation/tests/*myProjName*/output/test_002/")
std::string getTestPath(std::string hifFilePath) {
    DIR *dir;
    int num = 0;

    std::string outputDirName = "output/";

    // Test directory name (without counter)
    std::string testDirName = "test_";

    // Removing the filename from file.hif.xml path
    std::string path = removeFilenameFromPath(hifFilePath);

    // Output directory
    path = path + outputDirName;
    if (mkdir(path.c_str(), 0777) != -1)
        std::cout << "#> Output directory created." << std::endl << std::endl;


    // Open output directory
    if ((dir = opendir(path.c_str())) != NULL) {
        struct dirent *ent;
        std::string lastDirName = "";

        // For each element inside directory
        while ((ent = readdir(dir)) != NULL) {
            lastDirName = ent->d_name;

            // Catch stoi()'s exception when a directory name is:
            //      testDirName + alphabetical/special characters (not digits)
            try {
                // Get higher value (eg. test_020, test_007 --> 20) if directory name starts with testDirName
                if (lastDirName.rfind(testDirName, 0) == 0 && stoi(lastDirName.substr(5)) > num)
                    num = stoi(lastDirName.substr(5));

            } catch (const std::invalid_argument &e) {}     // If exception, skip directory
        }
        closedir(dir);
    }

    // n-th directory exists than create (n+1)-th directory
    ++num;

    // .../output/test_XXX
    path = add3DigitNumberToString(path + testDirName, num);


    if (mkdir(path.c_str(), 0777) == -1) {  // Directory not created
        std::cout << "#> ERROR DURING TEST_XXX DIRECTORY CREATION!" << std::endl
                  << "#>> Make sure the hif.xml file is inside .../approximation/tests/*proj_name*/ directory."
                  << std::endl
                  << "#>> If it won't go away, please backup output directory and delete it."
                  << std::endl << std::endl;
        std::exit(1);
    }

    return path + "/";
}


// Adds the 3-digit number to the string
std::string add3DigitNumberToString(std::string str, unsigned int n) {
    if (n < 10)
        str = str + "0";
    if (n < 100)
        str = str + "0";

    return str + std::to_string(n);
}


// Check the Hif.xml file path
// Hif.xml file should be inside ".../tests/*myProjName*/" directory
void checkHifXmlPath(std::string pathHifXmlFile) {
    std::string testDir = "/tests";

    // E.g. ".../approximation/tests/proj/myProj.hif.xml"
    std::string path(realpath(pathHifXmlFile.c_str(), NULL));
    // Path without hix.xml filename (e.g. ".../approximation/tests/proj/" without "myProj.hif.xml")
    path = path.substr(0, path.find_last_of("\\/"));
    // Path without hif.xml filename and project directory (e.g. ".../approximation/tests/" without "proj/")
    path = path.substr(0, path.find_last_of("\\/"));

    // Check if path has testDir
    if (testDir == path.substr(path.find_last_of("\\/")))
        return;


    std::cout << "#> ERROR! HIF.XML FILE WRONG PATH!" << std::endl
              << "#>> Make sure the hif.xml file is inside .../approximation/tests/*proj_name*/. directory"
              << std::endl << std::endl;

    std::exit(1);
}


// From test directory path to project directory path
// E.g. input: ".../tests/*myProjName*/output/test_027/"
//      output: ".../tests/*myProjName*/"
std::string getProjPathFromTestPath(std::string path) {

    // From ".../tests/*myProjName*/output/test_XXX/" to ".../tests/*myProjName*/output/test_XXX"
    path = path.substr(0, path.size() - 1);
    // From ".../tests/*myProjName*/output/test_XXX" to ".../tests/*myProjName*/output/"
    path = path.substr(0, path.find_last_of("/"));
    // From ".../tests/*myProjName*/test_XXX/output" to ".../tests/*myProjName*/"
    path = path.substr(0, path.size() - 6);
    // Now dir points where hif.xml and verilog files

    return path;
}
