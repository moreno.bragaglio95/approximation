#include "ast2ddgVisitor.hpp"


// Constructor
ast2ddgVisitor::ast2ddgVisitor() : hif::GuideVisitor() {
    this->dbmMap = new std::map<hif::DesignUnit *, DBM>();

    verbose = isVerbose();
}


// Constructor
ast2ddgVisitor::ast2ddgVisitor(std::map<hif::DesignUnit *, DBM> *dbmMap) {
    this->dbmMap = dbmMap;

    verbose = isVerbose();
}


// <LIBRARYDEFS> visitors
int ast2ddgVisitor::visitLibraryDef(hif::LibraryDef &o) {
    // For the LIBRARYDEFS xml tag there will not be visiting. Pass directly to the next tag
    return 0;
}


// <DESIGNUNIT> visitors
int ast2ddgVisitor::visitDesignUnit(hif::DesignUnit &o) {
    lastVisitedDU = &o;
    return hif::GuideVisitor::visitDesignUnit(o);
}


// <PORT> visitors
int ast2ddgVisitor::visitPort(hif::Port &o) {
    lastVisitedPort = &o;
    return hif::GuideVisitor::visitPort(o);
}


// <SIGNAL> visitors
int ast2ddgVisitor::visitSignal(hif::Signal &o) {
    lastVisitedSignal = &o;
    return GuideVisitor::visitSignal(o);
}


// <VALUETP> visitors
int ast2ddgVisitor::visitValueTP(hif::ValueTP &o) {
    lastVisitedValueTP = dynamic_cast<hif::Object *>(&o);
    return GuideVisitor::visitValueTP(o);
}


// <IDENTIFIER> visitors
//
// If inside an ASSIGN:
//      if the identifier is an index (e.g. "i" in "a[i]" or "a[i:j]"), add the arch "index --> vector" to the graph,
//      otherwise add the identifier to an handside (LHS or RHS) of the assign.
// If inside a SIGNAL, VALUETP or PORT: (this means that there are an EXPRESSION inside the SIGNAL/VALUETP/PORT)
//      add the arch "identifier --> signal/valueTP/port" to the graph.
int ast2ddgVisitor::visitIdentifier(hif::Identifier &o) {
    // If the identifier is inside <ASSIGN> and not <CONDITION>
    if (isInsideClassTag(&o, hif::CLASSID_ASSIGN) && !isInsideTagName(&o, conditionTagName)) {

        // Checks if "a[i]" or "a[j:i]"
        if (isInsideClassTag(&o, hif::CLASSID_MEMBER) || isInsideClassTag(&o, hif::CLASSID_SLICE)) {

            // Checks if it's the prefix ("a" in "a[i]"")
            if (isInsideTagName(&o, prefixTagName)) {
                lastVisitedPrefix = dynamic_cast<hif::Object *>(&o);
                addIdToHs(&o);


            } else if (isInsideTagName(&o, indexTagName) || isInsideClassTag(&o, hif::CLASSID_RANGE)) {
                // Checks if the identifier is inside brackets ("i" in "a[i]")

                dbmMap->operator[](lastVisitedDU).cdfg.add(&o, lastVisitedPrefix);
            }

        } else {
            addIdToHs(&o);
        }

    } else if (isInsideClassTag(&o, hif::CLASSID_SIGNAL)) {  // If inside <SIGNAL>
        dbmMap->operator[](lastVisitedDU).cdfg.add(&o, lastVisitedSignal);

    } else if (isInsideClassTag(&o, hif::CLASSID_VALUETP)) { // If inside <VALUETP>
        dbmMap->operator[](lastVisitedDU).cdfg.add(&o, lastVisitedValueTP);

    } else if (isInsideClassTag(&o, hif::CLASSID_PORT)) {    // If inside <PORT>
        dbmMap->operator[](lastVisitedDU).cdfg.add(&o, lastVisitedPort);
    }

    return GuideVisitor::visitIdentifier(o);
}


// Generic visitor, AFTER visit
int ast2ddgVisitor::AfterVisit(hif::Object &o) {
    if (dynamic_cast<hif::Assign *>(&o) != nullptr) {   // If Assign
        lastVisitedPrefix = nullptr;

        addHsToGraph();
        setLHS.clear();
        setRHS.clear();

    } else if (dynamic_cast<hif::Port *>(&o) != nullptr) {  // If Port
        lastVisitedPort = nullptr;

    } else if (dynamic_cast<hif::Signal *>(&o) != nullptr) {    // If Signal
        lastVisitedSignal = nullptr;

    } else if (dynamic_cast<hif::ValueTP *>(&o) != nullptr) {   // If ValueTP
        lastVisitedValueTP = nullptr;
    }

    return HifVisitor::AfterVisit(o);
}


// Print graph arches
void ast2ddgVisitor::printGraph() {
    dbmMap->operator[](lastVisitedDU).cdfg.printGraph();
}


// Check the position (LHS, RHS) and then add the identifier to the LeftHandSide set or the RightHandSide set
void ast2ddgVisitor::addIdToHs(hif::Identifier *o) {
    char hs = isLeftOrRightHandSide(o);

    if (hs == 'r')
        setRHS.insert(dynamic_cast<hif::Object *>(o));
    else if (hs == 'l')
        setLHS.insert(dynamic_cast<hif::Object *>(o));
}


// Add arches to the graph:
// RHS set objets --> LHS set object
void ast2ddgVisitor::addHsToGraph() {
    std::string stringLHS, stringRHS;

    for (it_setLHS = setLHS.begin(); it_setLHS != setLHS.end(); ++it_setLHS) {
        stringLHS = getObjName(*it_setLHS);

        for (it_setRHS = setRHS.begin(); it_setRHS != setRHS.end(); ++it_setRHS) {
            stringRHS = getObjName(*it_setRHS);

            if (stringLHS != stringRHS) {
                (*dbmMap)[lastVisitedDU].cdfg.add(dynamic_cast<hif::Object *>(*it_setRHS),
                                                  dynamic_cast<hif::Object *>(*it_setLHS));

                if (isVerbose()) {
                    std::cout << "ADD DDG: " << getObjName(dynamic_cast<hif::Object *>(*it_setRHS)) << " --> "
                              << getObjName(dynamic_cast<hif::Object *>(*it_setLHS)) << std::endl;
                }
            }
        }
    }
}
