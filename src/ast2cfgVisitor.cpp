#include "ast2cfgVisitor.hpp"


// Constructor
ast2cfgVisitor::ast2cfgVisitor() : hif::GuideVisitor() {
    this->dbmMap = new std::map<hif::DesignUnit *, DBM>();

    verbose = isVerbose();
}


// Constructor
ast2cfgVisitor::ast2cfgVisitor(std::map<hif::DesignUnit *, DBM> *dbmMap) {
    this->dbmMap = dbmMap;

    verbose = isVerbose();
}


// <LIBRARYDEFS> visitors
int ast2cfgVisitor::visitLibraryDef(hif::LibraryDef &o) {
    // For the LIBRARYDEFS xml tag there will not be visiting. Pass directly to the next tag
    return 0;
}


// <DESIGNUNIT> visitors
int ast2cfgVisitor::visitDesignUnit(hif::DesignUnit &o) {
    lastVisitedDU = &o;

    // New design unit, new cfgMap
    cfgMap.clear();

    return hif::GuideVisitor::visitDesignUnit(o);
}


// <IDENTIFIER> visitors
//
// If it's inside STATETABLE and not in STATE, this means it's in a sensitivity list
//      add it to the sensitivity list
//
// If it's inside CONDITION
//      add it to the top condition stack
//      if the always stack is not empty
//           if the condition is "directly" inside the Always block (no other blocks between them) *
//                add it to the influenced objects of the always stack
//           if "always @(*)" **
//                add it to the sensitivity list
//
// else if it's not inside ASSIGN or it's inside the LHS of an ASSIGN or it's inside a Ternary
//      add it to the cfgMap
//
// If it's inside ASSIGN and not inside CONDITION
//      if it's inside an Always block and a RHS of an ASSIGN
//           if the always block doens't have predetermined sensitivity list (always @(*))
//                add the identifier to the sensitivity list
//      if it's inside an Always block and a LHS of an ASSIGN
//           if the Assign is "directly" inside the Always block (no other blocks between them) *
//                add Identifier to influenced objects
//
//
// Note*: the identifier is influenced by the sensitivity list only if it's "directly" inside the Always block.
//      E.g.
//      always @(c) begin
//           a = 1'b0;              <-- a influenced by the sensitivity list (arch "sensitivity list objects --> a")
//           if(b == 1'b0)          <-- b influenced by the sensitivity list (arch "sensitivity list objects --> b")
//                if(c == 1'b0)     <-- c influenced by b (arch "b --> c")
//                     d == 1'b1;   <-- d influenced by c (arch "c --> d")
//      end
// Note**: The @* token adds to the sensitivity list all nets and variables that are read
//         by the statements in the always block.
int ast2cfgVisitor::visitIdentifier(hif::Identifier &o) {

    // If the Identifier is inside a sensitivity list
    if (isInsideClassTag(&o, hif::CLASSID_STATETABLE) && !isInsideClassTag(&o, hif::CLASSID_STATE))
        sensitivityList.push_back(&o);


    // If inside <CONDITION>
    if (isInsideTagName(&o, conditionTagName)) {
        // Collect variables of the current condition and update the stack

        if (!condStack.empty())
            condStack.top().insert(&o);

        // If inside <ALWAYS>
        if (!alwaysStack.empty()) {

            // If NOT inside <ASSIGN> and the condition is "directly" inside the Always block (no other blocks between them).
            if (!isInsideClassTag(&o, hif::CLASSID_ASSIGN) && std::get<3>(alwaysStack.top()) + 1 == condStack.size())
                std::get<1>(alwaysStack.top()).push_back(&o);   // Add Identifier to influenced objects

            // If always block doens't have sensitivity list (always @(*))
            if (!std::get<2>(alwaysStack.top()))
                std::get<0>(alwaysStack.top()).push_back(&o);   // Add Identifier to sensitivity list
        }


    } else if (!isInsideClassTag(&o, hif::CLASSID_ASSIGN) ||    // If NOT inside <CONDITION> and (NOT inside <ASSIGN>
               isLeftOrRightHandSide(&o) == 'l' ||  // or inside LHS of <ASSIGN>
               isInsideClassTag(&o, hif::CLASSID_WHEN)) {   // or inside <WHEN> (ternary) )

        // Creates a set of hif::Object and insert the object
        std::set<hif::Object *> newSet;
        newSet.insert(&o);

        // If condition stack not empty
        if (!condStack.empty())
            cfgMap.insert({newSet, condStack.top()});   // <condition_variables, body_variables>
    }



    // If inside <ASSIGN> and NOT inside <CONDITION>
    if (isInsideClassTag(&o, hif::CLASSID_ASSIGN) && !isInsideTagName(&o, conditionTagName)) {
        // No member/slice check

        // If inside Always block and RHS
        if (!alwaysStack.empty() && isLeftOrRightHandSide(&o) == 'r') {

            // If always block doens't have sensitivity list (always @(*))
            if (!std::get<2>(alwaysStack.top()))
                std::get<0>(alwaysStack.top()).push_back(&o);   // Add Identifier to sensitivity list

        } else if (!alwaysStack.empty() && isLeftOrRightHandSide(&o) == 'l') {   // If inside Always block and LHS

            // If the Assign is "directly" inside the Always block (no other blocks between them).
            if (std::get<3>(alwaysStack.top()) == condStack.size())
                std::get<1>(alwaysStack.top()).push_back(&o);   // Add Identifier to influenced objects
        }
    }

    return GuideVisitor::visitIdentifier(o);
}


// Generic visitor, BEFORE visit
//
// If it's a condition type:
//      push an empty set in the condition stack
// If it's a STATE (always block):
//      push in the always stack:
//          - the sensitivity list if it was found, otherwise an empty vector
//          - an empty vector (influenced object list)
//          - boolean, it's True if the sensitivity list was found
//          - size_t, condition stack size
bool ast2cfgVisitor::BeforeVisit(hif::Object &o) {

    // Conditions
    if (isCondition(&o)) {

        // Push new hif::Object set
        condStack.push(std::set<hif::Object *>());
    }

    // If visited object is a STATE (Always)
    if (dynamic_cast<hif::State *>(&o) != nullptr) {

        // No sensistivity list found (always @(*))
        if (sensitivityList.empty()) {
            // <Empty sens. list, empty influenced object list, it has NOT sens. list, condition stack size>
            alwaysStack.push({std::vector<hif::Object *>(), std::vector<hif::Object *>(), false, condStack.size()});
        } else {  // Sensitivity list found
            // <Sensitivity list, empty influenced object list, it has sens. list, condition stack size>
            alwaysStack.push({sensitivityList, std::vector<hif::Object *>(), true, condStack.size()});
        }
    }

    return HifVisitor::BeforeVisit(o);
}


// Generic visitor, AFTER visit
// If it's a condition type:
//       insert <top condition, second top condition> in cfgMap
// If it's a DESIGN UNIT:
//       add the CFG to the CDFG
// If it's a STATE (always block):
//       add the always block to the CDFG
// Note: cfgMap <condition_variables, body_variables>
int ast2cfgVisitor::AfterVisit(hif::Object &o) {

    // Conditions
    if (isCondition(&o)) {

        if (condStack.size() > 1) {
            // Link between stack and stack-1 conditions
            std::set<hif::Object *> topStack = condStack.top();
            condStack.pop();
            cfgMap.insert({topStack, condStack.top()});

        } else
            condStack.pop();


    } else if (dynamic_cast<hif::DesignUnit *>(&o) != nullptr) {    // If visited object is a DESIGN UNIT
        addCFGtoCDFG();
    }


    // If visited object is a STATE (Always)
    if (dynamic_cast<hif::State *>(&o) != nullptr) {

        // Adds arches to CDFG
        addAlwaysStackToCDFG();

        alwaysStack.pop();
        sensitivityList.clear();
    }

    return HifVisitor::AfterVisit(o);
}


// Print CFG
void ast2cfgVisitor::printCFG() {
    std::set<hif::Object *> cond, body;

    for (it_cfgMap = cfgMap.begin(); it_cfgMap != cfgMap.end(); ++it_cfgMap) {

        std::cout << "CONDITION IN SCOPE:" << std::endl;
        cond = it_cfgMap->first;

        for (hif::Object *c : cond)
            std::cout << " \t: " << getObjName(c) << std::endl;

        std::cout << "CONDITION IN PREVIOUS SCOPE:" << std::endl;
        body = it_cfgMap->second;

        for (hif::Object *b : body)
            std::cout << " \t: " << getObjName(b) << std::endl;

        std::cout << std::endl << std::endl;
    }
}


// Add CFG to CDFG:
// Add the arches "previous scope object --> current scope object" to the CDFG
void ast2cfgVisitor::addCFGtoCDFG() {
    std::set<hif::Object *> scope, previousScope;

    for (it_cfgMap = cfgMap.begin(); it_cfgMap != cfgMap.end(); ++it_cfgMap) {

        scope = it_cfgMap->first;
        previousScope = it_cfgMap->second;

        for (hif::Object *scopePtr : scope)

            for (hif::Object *previousScopePtr : previousScope) {
                dbmMap->operator[](lastVisitedDU).cdfg.add(previousScopePtr, scopePtr);

                if (verbose) {
                    std::cout << "ADD CFG: " << getObjName(previousScopePtr) << " --> "
                              << getObjName(scopePtr) << std::endl;
                }
            }
    }
}


// Add the current Always block to CDFG:
// Add the arches "sensitivity list object --> influenced object" to the CDFG
void ast2cfgVisitor::addAlwaysStackToCDFG() {
    hif::Object *sensObj, *inflObj;

    for (int i = 0; i < std::get<0>(alwaysStack.top()).size(); ++i)          // Sensitivity list
        for (int j = 0; j < std::get<1>(alwaysStack.top()).size(); ++j) {    // Influenced objects

            sensObj = std::get<0>(alwaysStack.top())[i];
            inflObj = std::get<1>(alwaysStack.top())[j];

            dbmMap->operator[](lastVisitedDU).cdfg.add(sensObj, inflObj);

            if (verbose)
                std::cout << "ADD ALW: " << getObjName(sensObj) << " --> " << getObjName(inflObj) << std::endl;
        }
}
