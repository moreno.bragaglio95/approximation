#include "Bmatrix.hpp"


// Constructor
Bmatrix::Bmatrix(std::vector<hif::Object *> columns,
                 short int orderMode,
                 std::string testDirPath,
                 hif::DesignUnit *rootDu,
                 std::string fileCounter) {

    this->columns = columns;                // Output
    this->columnNumber = columns.size();
    this->orderMode = orderMode;            // Order mode
    this->testDirPath = testDirPath;        // Test directory path
    this->rootDu = rootDu;                  // Root design unit
    this->fileCounter = fileCounter;        // File counter (e.g "003" in "file_1_003_tb.txt")

    maxDistance = 0;
    maxImpact = 0;
}


// Constructor
Bmatrix::Bmatrix(std::vector<hif::Object *> columns,
                 short int orderMode,
                 std::string testDirPath,
                 hif::DesignUnit *rootDu,
                 std::string fileCounter,
                 std::vector<bmTuple> bmatrix) {

    this->columns = columns;                // Output
    this->columnNumber = columns.size();
    this->orderMode = orderMode;            // Order mode
    this->testDirPath = testDirPath;        // Test directory path
    this->rootDu = rootDu;                  // Root design unit
    this->fileCounter = fileCounter;        // File counter (e.g "003" in "file_1_003_tb.txt")
    this->bmatrix = bmatrix;                // B matrix

    maxDistance = 0;
    setMaxDistance();

    maxImpact = 0;
    setMaxImpact();
}


// Return the column vector
std::vector<hif::Object *> *Bmatrix::getColumns() {
    return &columns;
}


// Add a row into the matrix:
//    - The coverage hit must be >= 0
//    - #dependencies must be equal to #column
// Also save the maximum distance (it will be used during Bmatrix printing).
void Bmatrix::add(hif::Object *statementObj,
                  size_t lineNumber,
                  std::string statementString,
                  std::vector<dependency> dependencies,
                  size_t coverageCounter,
                  float assertionImpact) {

    // If coverage counter == -1, DON'T add it
    if (coverageCounter == -1)
        return;

    // Check dependencies format
    if (dependencies.size() == columnNumber) {
        // Add to B matrix
        bmatrix.emplace_back(statementObj, lineNumber, statementString, dependencies, coverageCounter, assertionImpact);

        // Save the maximum distance of B matrix
        for (dependency dep : dependencies)
            if (dep.second > maxDistance)
                maxDistance = dep.second;

        // Save the maximum impact
        if (maxImpact < assertionImpact)
            maxImpact = assertionImpact;

    } else {    // Wrong dependencies format
        std::cout << "#> ERROR! WRONG DEPENDENCIES FORMAT!" << std::endl;
        std::exit(1);
    }
}


// Print B matrix:
//    - If orderMode = 3 or 4, get output priority
//    - Save paddingMax (longest DU name)
//    - Print intestation
//    - Print rows
void Bmatrix::printBmatrix(hif::DesignUnit *du) {

    // If B matrix has output columns
    if (columnNumber != 0) {

        // If necessary, for each output sets priority
        std::vector<std::pair<float, hif::Object *>> outputPriority;

        if (orderMode == 3)          // For each output, set priority (float)
            setOutputsWeight(&outputPriority, du);
        else if (orderMode == 4)     // For each output, set priority order (output importance)
            setOutputsImportance(&outputPriority, du);


        // Calculate longest Design Unit name
        int padding, paddingMax = 0;
        for (int i = 0; i < bmatrix.size(); ++i) {
            // DU name length + 3
            padding = getObjName(getDesignUnit(std::get<0>(bmatrix[i]))).length() + 3;

            if (paddingMax < padding)
                paddingMax = padding;
        }


        // Print as file
        std::ofstream file;
        std::streambuf *sb;

        file.open(testDirPath + "Bmatrix" + "_" + fileCounter
                  + "_" + std::to_string(orderMode)
                  + "_" + rootDu->getName()->c_str() + ".txt");
        sb = setCout(&file);


        // Intestation
        std::cout << "###### Module: " << getObjName(du) << " ######" << std::endl;


        // Output list
        std::cout << std::endl;
        printObjectVector(&columns, true, false);

        // Header: Out list
        std::cout << "\t";
        for (int i = 0; i < columnNumber; ++i)
            std::cout << "out" << std::left << std::setfill(' ') << std::setw(7) << i;  // 10-3=7

        // Header: Impact on assertion set
        if (std::get<5>(bmatrix[0]) != -1)
            std::cout << std::left << std::setfill(' ') << std::setw(15) << "Impact";

        // Header: Priority
        if (orderMode != 4)
            std::cout << std::left << std::setfill(' ') << std::setw(15) << "Method result";

        // Header: Coverage hit
        std::cout << std::left << std::setfill(' ') << std::setw(15) << "Hit";

        // Header: Design unit [15 or max (DU name length + 3)]
        if (paddingMax < 15)
            std::cout << std::left << std::setfill(' ') << std::setw(15) << "Design Unit";
        else
            std::cout << std::left << std::setfill(' ') << std::setw(paddingMax) << "Design Unit";

        // Header: Hif type, line number, statements
        std::cout << std::left << std::setfill(' ') << std::setw(15) << "Hif type"
                  << std::left << std::setfill(' ') << std::setw(15) << "Line number"
                  << std::left << std::setfill(' ') << std::setw(15) << "Statements" << std::endl;


        // Rows
        if (orderMode == 1)
            printRows_1(&outputPriority, paddingMax);
        else if (orderMode == 4)
            printRows_4(&outputPriority, paddingMax);
        else
            printRows_23(&outputPriority, paddingMax);


        std::cout << std::endl;
        setCout(sb);
        file.close();

    } else {
        // Print as file
        std::ofstream file;
        std::streambuf *sb;

        file.open(testDirPath + "Bmatrix"
                  + "_" + fileCounter + "_" + std::to_string(orderMode)
                  + "_" + rootDu->getName()->c_str() + ".txt");
        sb = setCout(&file);

        // Intestation
        std::cout << "###### Module: " << getObjName(du) << " ######" << std::endl << std::endl;
        std::cout << "#> Module doesn't have out/inout ports." << std::endl << std::endl;

        setCout(sb);
        file.close();
    }
}


// Print rows, orderMode = 1 (Influenced outputs number):
//    - Sort the B matrix in descending order based on the hit coverage value
//    - Sort the B matrix in ascending order based on priority
//    - Print each row
//
// B matrix order based on influenced outputs number.
// If same value, order based on hit coverage value.
// If same value, order based on HIF visit.
void Bmatrix::printRows_1(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                          int paddingMax) {

    // Sorting Bmatrix into ascending order (priority)
    std::sort(bmatrix.begin(), bmatrix.end(),
              [](bmTuple const &t1, bmTuple const &t2) {
                  return std::get<4>(t1) < std::get<4>(t2);
              });


    // Ordered Bmatrix: <impact, <priority (float), Bmatrix's tuple pointer>
    std::map<float, std::multimap<float, bmTuple *>> orderedBmatrix;

    // Sets priority for each row
    float tempMethodResult = 0, maxMethodResult = 0;
    for (int i = 0; i < bmatrix.size(); ++i) {
        tempMethodResult = getRowPriority(std::get<3>(bmatrix[i]), outputPriority);
        if (maxMethodResult < tempMethodResult)
            maxMethodResult = tempMethodResult;

        // Add row to orderedBmatrix (order based on impact)
        // orderedBmatrix[impact].insert(method result value, tuple)
        orderedBmatrix[std::get<5>(bmatrix[i])].insert({tempMethodResult, &bmatrix[i]});
    }


    // Print rows
    for (auto map : orderedBmatrix) {
        for (auto submap : map.second) {
            // Normalized priority
            printRow(submap.second, submap.first / maxMethodResult, paddingMax);
        }
    }
}


// Print rows, orderMode = 2 or 3 (Distances average or Distances weighted average):
//    - Sort the B matrix in ascending order based on the hit coverage value
//    - Sort the B matrix in ascending order based on priority
//    - Print each row (reverse iterator)
//
// B matrix order based on distances average (if orderMode = 2) or distances weighted average (if orderMode = 3).
// If same value, order based on hit coverage value.
// If same value, order based on HIF visit.
void Bmatrix::printRows_23(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                           int paddingMax) {

    // Sorting Bmatrix into ascending order (priority)
    std::sort(bmatrix.begin(), bmatrix.end(),
              [](bmTuple const &t1, bmTuple const &t2) {
                  return std::get<4>(t1) < std::get<4>(t2);
              });


    // Ordered Bmatrix: <impact, <priority (float), Bmatrix's tuple pointer>
    std::map<float, std::multimap<float, bmTuple *>> orderedBmatrix;

    // Sets priority for each row
    float tempMethodResult = 0;
    float minMethodResult = (maxDistance * 2) + 1;  // Looking for the minimum value (distances avg)
    for (int i = 0; i < bmatrix.size(); ++i) {
        tempMethodResult = getRowPriority(std::get<3>(bmatrix[i]), outputPriority);
        if (minMethodResult > tempMethodResult)
            minMethodResult = tempMethodResult;

        // Add row to orderedBmatrix (order based on impact)
        // orderedBmatrix[impact].insert(method result value, tuple)
        orderedBmatrix[std::get<5>(bmatrix[i])].insert({-tempMethodResult, &bmatrix[i]});
        // "-tempMethodResult": greatest distances avg in first position
    }


    // Print rows
    for (auto map : orderedBmatrix) {
        for (auto submap : map.second) {
            // Normalized priority
            printRow(submap.second, -1 * minMethodResult / submap.first, paddingMax);
        }
    }
}


// Print rows, orderMode = 4 (Outputs importance):
//    - Sort the B matrix in ascending order based on the hit coverage value
//    - Sort the B matrix in ascending order based on priority
//    - Print each row (reverse iterator)
//
// B matrix order based on the utmost importance output.
// If same value, order based on the second utmost importance output.
// If same value, order based on the third utmost importance output.
// ...
// If same value, order based on HIF visit.
void Bmatrix::printRows_4(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                          int paddingMax) {

    // Sorting Bmatrix into descending order (coverage hit)
    std::sort(bmatrix.begin(), bmatrix.end(),
              [](bmTuple const &t1, bmTuple const &t2) {
                  return std::get<4>(t1) > std::get<4>(t2);
              });


    // Bmatrix facsimile (without statement string)
    std::vector<std::tuple<hif::Object *, std::vector<dependency>, float>> bmatrix_red;

    // Fills bmatrix_red, based on output priority
    orderBmatrixByOutputImportance(&bmatrix_red, outputPriority);


    // Sorting Bmatrix into ascending order (impact)
    std::sort(bmatrix_red.begin(), bmatrix_red.end(),
              [](auto const &t1, auto const &t2) {
                  return std::get<2>(t1) > std::get<2>(t2);
              });


    int pos;
    for (int i = bmatrix_red.size() - 1; i >= 0; --i) {
        pos = getObjRowPosition(std::get<0>(bmatrix_red[i]));
        printRow(&bmatrix[pos], -1, paddingMax);
    }
}


// Print a single row of the B matrix:
//    - For each output print 1 (if statement influences that output), otherwise 0
//          and distances (if statement influences that output), otherwise 0
//          (e.g. 1/10    0/0    1/2)
//    - If orderMode != 4, print priority
//    - Print coverage hit
//    - Print design unit name
//    - Print Hif type
//    - Print line number
//    - Print statement string (line code read from Verilog file)
void Bmatrix::printRow(bmTuple *tuple,
                       float priority,
                       int paddingMax) {
    std::string row;
    std::cout << "\t";

    // For each output
    for (int j = 0; j < columnNumber; ++j) {
        // Does statement influence the output? (Yes = "1")
        if (std::get<3>(*tuple)[j].first)
            row = "1";
        else
            row = "0";

        // Distance
        row += "/" + std::to_string(std::get<3>(*tuple)[j].second);

        std::cout << std::left << std::setfill(' ') << std::setw(10) << row;
    }


    // Impact
    if (std::get<5>(bmatrix[0]) != -1)
        std::cout << std::left << std::setfill(' ') << std::setw(15) << std::setprecision(5)
                  << std::get<5>(*tuple) / maxImpact;

    // Priority
    if (orderMode != 4)
        std::cout << std::left << std::setfill(' ') << std::setw(15) << std::setprecision(5) << priority;

    // Coverage hit
    std::cout << std::left << std::setfill(' ') << std::setw(15) << std::to_string(std::get<4>(*tuple));

    // Design unit [15 or max(DU name length + 3)]
    if (paddingMax < 15)
        std::cout << std::left << std::setfill(' ') << std::setw(15)
                  << getObjName(getDesignUnit(std::get<0>(*tuple)));
    else
        std::cout << std::left << std::setfill(' ') << std::setw(paddingMax)
                  << getObjName(getDesignUnit(std::get<0>(*tuple)));

    // Hif type, line number
    std::cout << std::left << std::setfill(' ') << std::setw(15) << getClassIDString(std::get<0>(*tuple))
              << std::left << std::setfill(' ') << std::setw(15) << std::to_string(std::get<1>(*tuple));

    // Statements string
    std::cout << std::get<2>(*tuple) << std::endl;
}


// Return the priority of the row
float Bmatrix::getRowPriority(std::vector<dependency> deps,
                              std::vector<std::pair<float, hif::Object *>> *outputPriority) {

    switch (orderMode) {
        case 1:
            return getPr1_InfluencedOutputsNumber(deps);
        case 2:
            return getPr2_DistancesAverage(deps);
        case 3:
            return getPr3_DistancesWeightedAverage(deps, outputPriority);
        default:
            return -1;
    }
}


// Return the priority of the row (Influenced outputs number)
// Priority: Count of influenced output
//
// E.g. 3 outputs:
//    - 1/4    1/2    1/6   <-- priority: 3
//    - 0/0    1/1    0/0   <-- priority: 1
float Bmatrix::getPr1_InfluencedOutputsNumber(std::vector<dependency> deps) {

    float priority = -1;

    if (!deps.empty()) {
        priority = 0;

        // Count 1s in dependencies
        for (int j = 0; j < columnNumber; ++j)
            if (deps[j].first)
                priority++;
    }

    return priority;
}


// Return the priority of the row (Distances average)
// Priority: Average of distances
//           If statement doesn't influece the output, distance = (maxDistance * 2) + 1
//
// E.g. 3 outputs, maxDistance = 6:
//    - 1/4    1/2    1/6   <-- priority: 4.0000 = (4 + 2 + 6) / 3
//    - 0/0    1/1    0/0   <-- priority: 9.0000 = (13 + 1 + 13) / 3
float Bmatrix::getPr2_DistancesAverage(std::vector<dependency> deps) {

    float priority = -1;

    if (!deps.empty()) {
        priority = 0;

        // Sum distances
        for (int j = 0; j < columnNumber; ++j) {

            if (deps[j].first) {
                priority = priority + deps[j].second;
            } else {
                // Statement does not modify the output: this is the worst case.
                // That's why it doubles (+1) the worst calculated distance
                priority = priority + ((maxDistance) * 2) + 1;
            }
        }

        // Average
        priority = priority / columnNumber;
    }

    return priority;
}


// Return the priority of the row (Distances weighted average)
// Priority: Weighted average of distances
//           If statement doesn't influece the output, distance = [(maxDistance * 2) + 1] * weight
//
// E.g. 3 outputs, maxDistance = 6, weights = [0.5, 0.7, 1]:
//    - 1/4    1/2    1/6   <-- priority: 3.1333 = (4*0.5 + 2*0.7 + 6*1) / 3
//    - 0/0    1/1    0/0   <-- priority: 6.7333 = (13*0.5 + 1*0.7 + 13*1) / 3
float Bmatrix::getPr3_DistancesWeightedAverage(std::vector<dependency> deps,
                                               std::vector<std::pair<float, hif::Object *>> *outputWeight) {
    float priority = -1;

    if (!deps.empty()) {
        priority = 0;

        // Sum weighted distances
        for (int j = 0; j < columnNumber; ++j) {

            if (deps[j].first) {
                priority = priority + (deps[j].second * outputWeight->operator[](j).first);
            } else {
                // Statement does not modify the output: this is the worst case.
                // That's why it doubles (+1) the worst calculated distance
                priority = priority + (((maxDistance * 2) + 1) * outputWeight->operator[](j).first);
            }
        }

        // Weighted average
        priority = priority / columnNumber;
    }

    return priority;
}


// Given an output, returns its position (-1 if not found)
//
// E.g. output list: (out1, out2, out3, out4, out5)
//      input: *out3
//      output: 2
int Bmatrix::getOutputPosition(hif::Object *obj) {

    for (int pos = 0; pos < columnNumber; ++pos) {
        if (columns[pos] == obj)
            return pos;
    }

    return -1;
}


// Given a statement object, returns the row position (-1 if not in Bmatrix)
int Bmatrix::getObjRowPosition(hif::Object *obj) {

    for (int i = 0; i < bmatrix.size(); ++i) {
        if (obj == std::get<0>(bmatrix[i]))
            return i;
    }

    return -1;
}


// Return the output vector
// Note: columns = project's outputs
std::vector<hif::Object *> Bmatrix::getOutputs() {
    return columns;
}


// Return the unordered B matrix
std::vector<bmTuple> Bmatrix::getUnorderedBmatrix() {
    return bmatrix;
}


// Print outputs as a list
void Bmatrix::printObjectVector(std::vector<hif::Object *> *objs,
                                bool isBulletedList,
                                bool isNumberedList) {

    std::cout << std::endl << "[" << getObjName(getDesignUnit(objs->operator[](0)))
              << "] Outputs list:" << std::endl;


    for (int i = 0; i < objs->size(); ++i) {
        // Different types of lists
        if (isBulletedList)
            std::cout << "\tout" << i << ". \t";
        else if (isNumberedList)
            std::cout << "\t" << i << ". \t";
        else
            std::cout << "\t- ";

        std::cout << getObjName(objs->operator[](i)) << std::endl;
    }

    std::cout << std::endl << std::endl;
}


// Return the minimum dependency of the n-th output (n = outputPos) from a specific bmatrix_red
// Note: 0/0 has the lowest value. E.g. 1/1 > 1/99 > 0/0
std::pair<bool, int>
Bmatrix::getMinimumDependency(std::vector<std::tuple<hif::Object *, std::vector<dependency>, float>> *bmatrix_red_tmp,
                              int position) {
    std::pair<bool, int> dep;
    int min = maxDistance + 1;

    // For each pair in bmatrix_red_tmp
    for (int i = 0; i < bmatrix_red_tmp->size(); ++i) {
        // Get dependency (n-th, n = position)
        dep = std::get<1>(bmatrix_red_tmp->operator[](i))[position];

        // Save if the corresponding statement affects the output (i.e. not "0/0") and if it's minor
        if (dep.first && dep.second < min)
            min = dep.second;
    }


    if (min != maxDistance + 1)
        return {true, min};
    else    // If bmatrix_red_tmp is empty or has only "0/0"
        return {false, 0};
}


// Sets weight (0 < w <= 1) of each outputs
// Note: w = 1 means that the output has the highest importance
void Bmatrix::setOutputsWeight(std::vector<std::pair<float, hif::Object *>> *outputWeight,
                               hif::DesignUnit *du) {
    float priority;
    std::string line;

    printObjectVector(&columns, false, false);

    std::cout << "Weight selection (0 < n <= 1.0):" << std::endl;


    // For each output
    for (int i = 0; i < columns.size(); ++i) {

        // Until an output is selected
        while (true) {
            std::cout << std::endl << "> Priority " << getObjName(columns[i]) << ": ";

            // Read and clear std::cin
            getline(std::cin, line);
            std::cin.clear();

            std::stringstream ss(line);

            // Input check
            if (ss >> priority &&   // Get priority
                line.find_first_not_of("0123456789.") == std::string::npos &&   // Only digits and '.'
                line.find_first_of(".") == line.find_last_of(".") &&    // At most one '.'
                priority > 0 && priority <= 1) {    // Priority range: 0 < priority <= 1
                break;
            }

            std::cout << "Please enter a Float only (0 < n <= 1.0)." << std::endl;
        }
        std::cin.clear();

        // Save priority
        outputWeight->push_back({priority, columns[i]});
    }
}


// Set the importance of the outputs
// The first output in outputImportance has the higher importance, the last one has lower importance.
void Bmatrix::setOutputsImportance(std::vector<std::pair<float, hif::Object *>> *outputImportance,
                                   hif::DesignUnit *du) {
    outputImportance->clear();

    int pos;
    std::string line;

    // Output vector,
    std::vector<hif::Object *> outputs = columns;


    // Each cycle outputs vecotr gets smaller
    for (int i = 0; i < columns.size() - 1; ++i) {

        // Print output vector
        printObjectVector(&outputs, false, true);

        // Until an output is selected
        while (true) {
            std::cout << "Select output with higher priority (number): ";

            // Read and clear std::cin
            getline(std::cin, line);
            std::cin.clear();

            std::stringstream ss(line);
            if (ss >> pos &&    // Get position
                line.find_first_not_of("0123456789") == std::string::npos &&    // Only digits
                pos >= 0 && pos <= outputs.size() - 1)  // Priority range: 0 < pr <= n-1 (n = #remaining_outputs)
                break;

            std::cout << "Please enter an Integer only (0 <= n <= " << outputs.size() - 1 << ")." << std::endl;
        }
        std::cin.clear();

        // Save selected output and erase it from outputs
        outputImportance->push_back({i, outputs[pos]});
        outputs.erase(outputs.begin() + pos);
    }

    // Add last output (it skips 1-output selection)
    outputImportance->push_back({columns.size() - 1, outputs[0]});
}


// Orders the B matrix based on output importance.
// The more it's the importance, the more it's the influence on the B matrix order.
//
// E.g. outputImportance: (out1, out3, out2)
//    - 1/3    1/3    1/5
//    - 1/3    1/4    1/5
//    - 1/3    1/3    0/0
//    - 1/5    1/1    1/1
// Note: outputImportance, the first element has the utmost importance
void Bmatrix::orderBmatrixByOutputImportance(
        std::vector<std::tuple<hif::Object *, std::vector<dependency>, float>> *bmatrix_red,
        std::vector<std::pair<float, hif::Object *>> *outputImportance) {

    // Support vectors
    std::vector<std::tuple<hif::Object *, std::vector<dependency>, float>> bmatrix_red2;

    // Fill bmatrix_red
    for (int i = 0; i < bmatrix.size(); ++i) {
        bmatrix_red->emplace_back(std::make_tuple(std::get<0>(bmatrix[i]),
                                                  std::get<3>(bmatrix[i]),
                                                  std::get<5>(bmatrix[i])));
    }

    std::pair<bool, int> minDep, dep;
    int outputPos;

    // For each output (outputImportance order)
    for (int j = outputImportance->size() - 1; j >= 0; --j) {
        bmatrix_red2.clear();

        // Get output posizion (e.g. {out4, out2, out7}, out2 position = 1)
        // Note: outputImportance has a different order than outputs in Bmatrix
        outputPos = getOutputPosition(outputImportance->operator[](j).second);

        // Until bmatrix_red is not empty
        while (!bmatrix_red->empty()) {

            // Each cycle bmatrix_red gets smaller
            for (int i = 0; i < bmatrix_red->size();) {
                // Get the minimum value from the remaining objects (e.g. 1/1, then 1/3, then 0/0)
                minDep = getMinimumDependency(bmatrix_red, outputPos);

                // For each remaining objects
                for (int k = 0; k < bmatrix_red->size();) {

                    // Get dependency
                    dep = std::get<1>(bmatrix_red->operator[](k))[outputPos];

                    // If minimun
                    if (dep.second == minDep.second && dep.first == minDep.first) {

                        // Push it into bmatrix_red2 and erase it from bmatrix_red
                        bmatrix_red2.push_back(bmatrix_red->operator[](k));
                        bmatrix_red->erase(bmatrix_red->begin() + k);

                    } else { // Otherwise erase() cause object-skip or exceptions
                        ++k;
                    }
                }
            }
        }

        *bmatrix_red = bmatrix_red2;
    }
}


// Set maxDistance (the maximum dependency distance in the B matrix)
void Bmatrix::setMaxDistance() {
    for (int i = 0; i < bmatrix.size(); ++i) {
        // Save the maximum distance of B matrix
        for (dependency dep : std::get<3>(bmatrix.operator[](i)))
            if (dep.second > maxDistance)
                maxDistance = dep.second;
    }
}


// Set maxImpact (the maximum impact in the B matrix)
void Bmatrix::setMaxImpact() {
    for (int i = 0; i < bmatrix.size(); ++i) {
        // Save the maximum impact
        if (maxImpact < std::get<5>(bmatrix.operator[](i)))
            maxImpact = std::get<5>(bmatrix.operator[](i));
    }
}
