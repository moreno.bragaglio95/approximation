#include "Graph.hpp"


// Constructor
Graph::Graph() = default;


// Print the graph:
// E.g.:
// #arch = 27
//    nodeA --> nodeB
//    nodeE --> nodeF
//    ...
void Graph::printGraph() {

    int i = 0;
    std::string firstDuName, firstObjName;

    // # arch
    std::cout << "#arch = " << size() << std::endl;

    // From each arch (e.g. nodeA --> nodeB)
    for (it_graph = graph.begin(); it_graph != graph.end(); it_graph++) {

        // Get design unit name and object (e.g. nodeA) name
        firstDuName = getObjName(getDesignUnit(it_graph->first));
        firstObjName = getObjName(it_graph->first);

        // (e.g. for each nodeB)
        for (i = 0; i < it_graph->second.size(); ++i)
            std::cout << "\t[" << firstDuName << "] " << firstObjName << " --> "
                      << "[" << getDesignUnit(it_graph->second[i])->getName()->c_str()
                      << "] " << getObjName(it_graph->second[i]) << std::endl;
    }
}


// Return the graph's size (#arch).
int Graph::size() {
    int size = 0;

    for (it_graph = graph.begin(); it_graph != graph.end(); ++it_graph)
        size += it_graph->second.size();

    return size;
}


// Add the arch into the graph.
//
// Avoids:
//   - loops ("nodeA --> nodeA"),
//   - duplicate archs,
//   - homonymous node: same node but different pointer.
//
// INPUT: nodeA, nodeB (arch = "nodeA --> nodeB")
// OUTPUT: True if arch added to the graph
bool Graph::add(hif::Object *fromNode, hif::Object *toNode) {

    // Avoid "[moduleA] i --> [moduleA] i"
    if (areSameObject(fromNode, toNode))
        return false;

    // During the HIF exploration it's possible to find the same object with different pointer.
    // This avoids duplicate graph keys.
    hif::Object *fromHnode = getHomonymousNode(fromNode);   // H, homonymous
    hif::Object *toHnode = getHomonymousNode(toNode);

    // If the arch already exists, DO NOT add
    if (!exist(fromHnode, toHnode)) {
        graph[fromHnode].push_back(toHnode);
        return true;
    }

    return false;
}


// Return the homonymous node (same name, same design unit).
// If not found in the graph, return the input node.
hif::Object *Graph::getHomonymousNode(hif::Object *node) {

    // For each arch (e.g. nodeA --> nodeB)
    for (it_graph = graph.begin(); it_graph != graph.end(); ++it_graph) {

        // For each nodeA
        if (areSameObject(it_graph->first, node)) {
            return it_graph->first;
        }

        // For each nodeB
        for (hif::Object *nodeB : it_graph->second)
            if (areSameObject(nodeB, node))
                return nodeB;
    }

    return node;
}


// Remove the arch "fromNode --> toNode" from the graph
//
// INPUT: nodeA, nodeB (arch = "nodeA --> nodeB")
// OUTPUT: True if arch removed to the graph
bool Graph::remove(hif::Object *fromNode, hif::Object *toNode) {

    // If exist at least an arch "fromNode --> nodeX"
    if (!graph[fromNode].empty()) {

        // For each "nodeX"
        for (int i = 0; i < graph[fromNode].size();) {

            // If "nodeX" == "toNode", erase the arch
            if (areSameObject(graph[fromNode][i], toNode)) {
                graph[fromNode].erase(graph[fromNode].begin() + i);
                return true;
            } else {
                ++i;
            }
        }
    }

    return false;
}


// Return True if the arch "fromNode --> toNode" is inside the graph
bool Graph::exist(hif::Object *fromNode, hif::Object *toNode) {

    // If exist at least an arch "fromNode --> nodeX"
    if (!graph[fromNode].empty()) {

        // For each "nodeX"
        for (int i = 0; i < graph[fromNode].size(); ++i) {

            // If "nodeX" == "toNode", return True
            if (areSameObject(graph[fromNode][i], toNode))
                return true;
        }
    }

    return false;
}


// Retrieve the list of all the objects related to the node.
//
// isFirst: True if you want *node as the first element of the arch,
//          False if you want *node as the second element of the arch
//
// E.g. isFirst = False
//      For loop... e.g. arch "a --> b"
//          If *node == "b" then "a" is added to the output.
std::multimap<hif::Object *, hif::Object *> Graph::getArchesFromNode(hif::Object *node, bool isFirst) {

    std::multimap<hif::Object *, hif::Object *> arches;
    int i;

    if (isFirst) {
        // Looking in "fromNode" (e.g "fromNode --> toNode")
        for (i = 0; i < graph[node].size(); ++i)
            arches.insert({node, graph[node][i]});

    } else {
        // Looking in "toNode" (e.g "fromNode --> toNode")
        for (it_graph = graph.begin(); it_graph != graph.end(); ++it_graph) {
            for (i = 0; i < it_graph->second.size(); ++i)

                if (it_graph->second[i] == node)
                    arches.insert({it_graph->first, node});
        }
    }

    return arches;
}


// Return graph's iterator, begin()
std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator Graph::graphBegin() {
    return graph.begin();
}


// Return graph's iterator, end()
std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator Graph::graphEnd() {
    return graph.end();
}


// Merge the graph fromGraph into the current graph:
//    - module instantiation arches (argument-port) added to the current graph (see below)
//    - each arch of fromGraph is copied into the current graph
void Graph::mergeGraph(Graph *fromGraph,
                       std::vector<std::tuple<hif::Object *, hif::Object *, hif::PortDirection>> *objBindVector) {

    hif::PortDirection portDir;
    int i;

    // fromGraph iterator
    std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator it_fromGraph;


    // Module instantiation arches (argument-port) added to the current graph
    // E.g. module A calls module B:
    // if input port: varA --> varB,
    // if output: varA <-- varB,
    // if inoutput: varA --> varB and varA <-- varB
    for (i = 0; i < objBindVector->size(); ++i) {

        // Get port direction
        portDir = std::get<2>(objBindVector->operator[](i));

        if (portDir == hif::dir_in || portDir == hif::dir_inout)     // varA --> varB
            add(std::get<1>(objBindVector->operator[](i)), std::get<0>(objBindVector->operator[](i)));

        if (portDir == hif::dir_out || portDir == hif::dir_inout)    // varA <-- varB
            add(std::get<0>(objBindVector->operator[](i)), std::get<1>(objBindVector->operator[](i)));
    }

    // Adds each fromGraph's arch
    for (it_fromGraph = fromGraph->graphBegin(); it_fromGraph != fromGraph->graphEnd(); ++it_fromGraph)
        for (i = 0; i < it_fromGraph->second.size(); ++i)
            add(it_fromGraph->first, it_fromGraph->second[i]);
}


// Count the minimum distance (minimum number of arches) from objStart to objTarget
const dependency Graph::minimumDistance(hif::Object *objStart, hif::Object *objTarget) {

    // Homonymous nodes
    hif::Object *objHstart = getHomonymousNode(objStart);
    hif::Object *objHtarget = getHomonymousNode(objTarget);

    // If already calculated
    if (minDistances.find(objHstart) != minDistances.end())
        if (minDistances[objHstart].find(objHtarget) != minDistances[objHstart].end())
            return minDistances[objHstart][objHtarget];


    // Calc
    int maxDistance = size() + 1;  // Max distance + 1
    dependency dep = {false, 0}; // No influence, distance = 0
    dependency minDep = minimumDistanceRecursive(dep, {true, maxDistance},
                                                 objHstart, objHtarget, std::vector<hif::Object *>());


    // If distance found
    if (minDep.second < maxDistance)
        dep = minDep;

    // Save dependency
    minDistances[objHstart][objHtarget] = dep;

    return dep;
}


// Recursion:
//    - if (current distance > mininumum distance found):
//        the current (incomplete/complete) path is longer than the minimum path, return the mininumum distance found
//    - if (current object == target object)
//        path found, return the current distance
//    - otherwise
//        recursion (for each "toNode", where "toNode" is "objCurrent --> toNode");
//        if a path is found, return the minumum distance
dependency Graph::minimumDistanceRecursive(dependency currentDeps,                      // Current distance
                                           dependency minDeps,                          // Minimum distance calculated
                                           hif::Object *objCurrent,                     // Current object
                                           hif::Object *objTarget,                      // Target object
                                           std::vector<hif::Object *> currentPath) {    // Cuurent path

    if (currentDeps.second > minDeps.second) {
        // Current path longer than minimun path
        return minDeps;


    } else if (areSameObject(objTarget, objCurrent)) { // If the current node is the target node
        currentDeps.first = true;
        return currentDeps;


    } else {
        std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator it_graph2;
        dependency tempDep;

        // objCurrent added to the path
        currentPath.push_back(objCurrent);


        // For each arch
        for (it_graph2 = graph.begin(); it_graph2 != graph.end(); ++it_graph2) {

            // If objCurrent is equal to it_graph2->first
            if (areSameObject(it_graph2->first, objCurrent)) {

                // For each toNode (e.g. "it_graph2->first --> toNode")
                for (int i = 0; i < it_graph2->second.size(); ++i) {
                    hif::Object *nextNode = it_graph2->second[i];

                    // If nextNode not in the current path (avoiding duplicates)
                    if (!isInsideVector(nextNode, &currentPath)) {

                        // If path nextNode-targetNode already calculated
                        if (minDistances.find(nextNode) != minDistances.end() &&
                            minDistances[nextNode].find(objTarget) != minDistances[nextNode].end()) {

                            if (!minDistances[nextNode][objTarget].first) { // nextNode-...->targetNode doesn't exists
                                tempDep = {false, 0};
                            } else {
                                // Current distance + 1 (currentNode --> nextNode) + nextNode-targetNode distance
                                tempDep = {true, currentDeps.second + 1 + minDistances[nextNode][objTarget].second};
                            }

                        } else {
                            // Recursion
                            tempDep = minimumDistanceRecursive({currentDeps.first, currentDeps.second + 1},
                                                               minDeps, it_graph2->second[i],
                                                               objTarget, currentPath);
                        }

                        // Save minimum distance
                        if (tempDep.first && tempDep.second < minDeps.second)
                            minDeps = tempDep;
                    }
                }
            }
        }
    }

    return minDeps;
}
