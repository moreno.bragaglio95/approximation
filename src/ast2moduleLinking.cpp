#include "ast2moduleLinking.hpp"


// Constructor
ast2moduleLinking::ast2moduleLinking() : hif::GuideVisitor() {
    this->dbmStruct = new std::map<hif::DesignUnit *, DBM>();
}


// Constructor
ast2moduleLinking::ast2moduleLinking(std::map<hif::DesignUnit *, DBM> *dbmStruct) {
    this->dbmStruct = dbmStruct;
}


// <LIBRARYDEFS> visitors
int ast2moduleLinking::visitLibraryDef(hif::LibraryDef &o) {
    // For the LIBRARYDEFS xml tag there will not be visiting. Pass directly to the next tag
    return 0;
}


// <DESIGNUNIT> visitors
int ast2moduleLinking::visitDesignUnit(hif::DesignUnit &o) {

    // New design unit, new structures
    dus.clear();
    moduleInstWithoutPorts.clear();
    moduleInstWithPorts.clear();

    lastVisitedDU = &o;

    return hif::GuideVisitor::visitDesignUnit(o);
}


// <IDENTIFIER> visitors
int ast2moduleLinking::visitIdentifier(hif::Identifier &o) {

    // If inside <PORTASSIGN>
    if (portAssignVisited) {

        // If PortAssign without name (argument without port)
        if (strcmp(lastVisitedPortAssign->getName()->getString(), "(no name)") == 0) {
            // Then verilog code is kind of: moduleName(param1, param2, ...)
            saveViewref_Ident(&o);
        } else {
            // Then verilog code is kind of: moduleName(.port1(param1), .port2(param2), ...)
            savePort_Ident(&o);
        }
    }
    return GuideVisitor::visitIdentifier(o);
}


// <PORT> visitors
int ast2moduleLinking::visitPort(hif::Port &o) {
    saveDU_Ports(&o);
    return hif::GuideVisitor::visitPort(o);
}


// <PORTASSIGN> visitors
int ast2moduleLinking::visitPortAssign(hif::PortAssign &o) {
    lastVisitedPortAssign = &o;
    portAssignVisited = true;
    return hif::GuideVisitor::visitPortAssign(o);
}


// <VIEWREFERENCE> visitors
int ast2moduleLinking::visitViewReference(hif::ViewReference &o) {
    lastVisitedViewRef = &o;
    return GuideVisitor::visitViewReference(o);
}


// Generic visitor, BEFORE visit
bool ast2moduleLinking::BeforeVisit(hif::Object &o) {

    // If it's a value (BitValue, IntValue, ...)
    if (isValue(&o))
        saveValues(&o);

    return HifVisitor::BeforeVisit(o);
}


// Generic visitor, AFTER visit
//
// If it's a PORTASSIGN without name (arguments without ports):
//      save the arguments of the visited PortAssign
// If it's a DESIGN UNIT:
//      save the data of the visited Design unit
int ast2moduleLinking::AfterVisit(hif::Object &o) {

    // If visited object is a PORTASSIGN
    if (dynamic_cast<hif::PortAssign *>(&o) != nullptr) {
        portAssignVisited = false;

        // If PortAssign without name
        if (strcmp(lastVisitedPortAssign->getName()->getString(), "(no name)") != 0) {

            // For each element in moduleInstWithPorts_internal
            for (it_moduleInstWithPorts_internal = moduleInstWithPorts_internal.begin();
                 it_moduleInstWithPorts_internal != moduleInstWithPorts_internal.end();
                 ++it_moduleInstWithPorts_internal) {

                // For each element in the vector
                for (int i = 0; i < it_moduleInstWithPorts_internal->second.size(); ++i) {

                    // Adds it in moduleInstWithPorts of the design unit lastVisitedViewRef
                    moduleInstWithPorts[lastVisitedViewRef].operator[](
                            it_moduleInstWithPorts_internal->first).push_back(
                            it_moduleInstWithPorts_internal->second.operator[](i));
                }
            }
        }

        moduleInstWithPorts_internal.clear();

    } else if (dynamic_cast<hif::DesignUnit *>(&o) != nullptr) { // If visited object is a DESIGN UNIT
        // Save data
        dbmStruct->operator[](dynamic_cast<hif::DesignUnit *>(&o)).dus = dus;
        dbmStruct->operator[](dynamic_cast<hif::DesignUnit *>(&o)).moduleInstWithPorts = moduleInstWithPorts;
        dbmStruct->operator[](dynamic_cast<hif::DesignUnit *>(&o)).moduleInstWithoutPorts = moduleInstWithoutPorts;
    }

    return HifVisitor::AfterVisit(o);
}


// Print module linking data:
//  - Ports
//  - Module instantiation, variables with port name
//      moduleA A (.in(var1))
//  - Module instantiation, variables without port name
//      moduleA A (var1)
void ast2moduleLinking::printMap() {
    std::string rows;

    // Print map of [hif::DesignUnit - hif::Port]
    rows = " ==== [MODULE - PORTS] ==== \n";
    rows = rows + " \t[ " + lastVisitedDU->getName()->getString() + " \t --> ";

    for (int i = 0; i < dus.size(); ++i) {
        rows = rows + dus[i]->getName()->c_str() + " ";
    }
    rows = rows + "]\n\n";
    std::cout << rows;


    // Print map of [hif::PortAssign - hif::Identifier]
    std::map<hif::PortAssign *, std::vector<hif::Object *>> internalMap;
    rows = " ==== [MODULE CALL - PASSED VARIABLES WITH PORT NAME] ==== \n";

    for (it_moduleInstWithPorts = moduleInstWithPorts.begin(); it_moduleInstWithPorts != moduleInstWithPorts.end();
         ++it_moduleInstWithPorts) {

        internalMap = it_moduleInstWithPorts->second;
        rows = rows + " \t[ " + it_moduleInstWithPorts->first->getDesignUnit()->getString() + " \t --> ";

        for (it_moduleInstWithPorts_internal = internalMap.begin();
             it_moduleInstWithPorts_internal != internalMap.end();
             ++it_moduleInstWithPorts_internal) {

            for (int i = 0; i < it_moduleInstWithPorts_internal->second.size(); ++i) {
                rows = rows + "(" + getObjName(it_moduleInstWithPorts_internal->first)
                       + "/" + getObjName(it_moduleInstWithPorts_internal->second.operator[](i)) + ") ";
            }
        }
        rows = rows + "]\n";
    }
    rows = rows + "\n";
    std::cout << rows;


    // Print map of [hif::ViewReference - hif::Identifier]
    rows = " ==== [MODULE CALL - PASSED VARIABLES WITHOUT PORT NAME] ==== \n";

    for (it_moduleInstWithoutPorts = moduleInstWithoutPorts.begin();
         it_moduleInstWithoutPorts != moduleInstWithoutPorts.end(); ++it_moduleInstWithoutPorts) {
        rows = rows + " \t[ " + it_moduleInstWithoutPorts->first->getDesignUnit()->getString() + " \t --> ";
        for (auto &port: it_moduleInstWithoutPorts->second) {

            // Inside a portassign tag can be found identifier, bit, int, char...
            if (dynamic_cast<hif::Identifier *>(port) != nullptr) {
                rows = rows + getObjName(port) + " ";

            } else if (isValue(port)) {
                rows = rows + getValueAsString(port) + " ";
            }
        }

        rows = rows + "]\n";
    }
    rows = rows + "\n";
    std::cout << rows;
}


// Save the value of the object
void ast2moduleLinking::saveValues(hif::Object *o) {

    // If it's inside <PORTASSIGN>
    if (portAssignVisited) {

        // If PortAssign without name (arguments without ports)
        if (strcmp(lastVisitedPortAssign->getName()->getString(), "(no name)") == 0) {
            // Then verilog code is kind of: moduleName(param1, param2, ...)
            saveViewref_Ident(o);
        }
    }
}


// Save the Design Unit port
void ast2moduleLinking::saveDU_Ports(hif::Port *o) {
    dus.push_back(o);
}


// Save the Port identifier
void ast2moduleLinking::savePort_Ident(hif::Object *o) {
    std::vector<hif::Object *> *lastVisitedModule = &moduleInstWithPorts_internal[lastVisitedPortAssign];

    // If o is an index, adds it at the end
    if (isIndex(o))
        lastVisitedModule->push_back(o);
    else // Otherwise (it's a prefix) adds it at the begin
        lastVisitedModule->insert(lastVisitedModule->begin(), o);
}


// Save the Viewreference identifier
void ast2moduleLinking::saveViewref_Ident(hif::Object *o) {
    moduleInstWithoutPorts[lastVisitedViewRef].push_back(o);
}
