#include "parseLine.hpp"

ParseLine::ParseLine(int argc, char *argv[]) :
        CommandLineParser(),
        uint_prova(5) {
    addToolInfos(
            // toolName, copyright
            "approximation", "Copyright (C) EDALab - Networked Embedded Systems (www.edalab.it)",
            // description
            "Parse a Verilog file and generates a AST.",
            // synopsys
            "approximation [OPTIONS] <HIF FILE>",
            // notes
            "- The default output directory name is hif2sc_out.\n"
            "\n"
            "Email: info@hifsuite.com    Site: www.hifsuite.com"
    );

    addHelp();
    addVersion();
    //addVerbose();
    addOutputDirectory();
    addOption('u', "uint_prova", true, true, "Get the uint. ");
    addOption('d', "--directory", true, true, "Set directory where design files are located. ");
    addOption('V', "--verbose", false, true, "Enables verbose output.", "");

    parse(argc, argv);

    _validateArguments();
}

uint64_t ParseLine::getUintProva() const {
    return uint_prova;
}

void ParseLine::_validateArguments() {
    if (!_options['h'].value.empty())
        printHelp();
    if (!_options['v'].value.empty())
        printVersion();

    if (!_options['V'].value.empty())
        setVerbose(true, true);
    else
       setVerbose(true, false);

    if (_files.empty()) {
        messageError("HIF input file missing.\n"
                     "Try 'approximation --help' for more information", nullptr, nullptr);
    }

    if (!_options['u'].value.empty()) {
        std::stringstream ss;
        ss << _options['u'].value;
        ss >> uint_prova;
    }
}