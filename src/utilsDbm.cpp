#include "utilsDbm.hpp"


// Given dbmMap, sets inDUs and outDus for each design unit
//
// std::set<hif::DesignUnit *> inDUs: design units that instantiates the current design unit
// std::set<hif::DesignUnit *> outDUs: design units instantiated by the current design unit.
//
// When it found a module instantiation (with or without ports):
//    - it adds the instantiated design unit to the outDUs of the current design unit
//    - it adds the current design unit to the inDUs of the instantiated design unit.
void setInDusOutDus(std::map<hif::DesignUnit *, DBM> *dbmMap) {

    bool verbose = isVerbose();

    // moduleInstWithPorts iterator
    std::map<hif::ViewReference *, std::map<hif::PortAssign *, std::vector<hif::Object *>>>::iterator it_miWithPorts;

    // moduleInstWithoutPorts iterator
    std::map<hif::ViewReference *, std::vector<hif::Object *>>::iterator it_miWithoutPorts;

    // dbmMap iterator
    std::map<hif::DesignUnit *, DBM>::iterator it_dbmMap;

    hif::DesignUnit *outerDU;


    // For each dbmMap's element
    for (it_dbmMap = dbmMap->begin(); it_dbmMap != dbmMap->end(); ++it_dbmMap) {

        // For each moduleInstWithPorts' element
        for (it_miWithPorts = it_dbmMap->second.moduleInstWithPorts.begin();
             it_miWithPorts != it_dbmMap->second.moduleInstWithPorts.end(); ++it_miWithPorts) {

            // Finding ViewReference's DesignUnit
            outerDU = findDesignUnitFromViewReference(it_miWithPorts->first, dbmMap);

            if (outerDU != nullptr) {

                // If moduleA instantiate moduleB:
                //      moduleA.outDUs <-- moduleB
                //      moduleB.inDUs  <-- moduleA
                dbmMap->operator[](it_dbmMap->first).outDUs.insert(outerDU);
                dbmMap->operator[](outerDU).inDUs.insert(it_dbmMap->first);
            } else {

                // Warning message
                if (verbose)
                    std::cout << "#WP> MODULE NOT FOUND! " << it_miWithPorts->first->getDesignUnit()->c_str()
                              << " ( " << it_dbmMap->first->getName()->c_str() << " ) " << std::endl;
            }
        }


        // For each moduleInstWithoutPorts' element
        for (it_miWithoutPorts = it_dbmMap->second.moduleInstWithoutPorts.begin();
             it_miWithoutPorts != it_dbmMap->second.moduleInstWithoutPorts.end(); ++it_miWithoutPorts) {

            // Finding ViewReference's DesignUnit
            outerDU = findDesignUnitFromViewReference(it_miWithoutPorts->first, &(it_miWithoutPorts->second), dbmMap);

            if (outerDU != nullptr) {

                // If moduleA instantiate moduleB:
                //      moduleA.outDUs <-- moduleB
                //      moduleB.inDUs  <-- moduleA
                dbmMap->operator[](it_dbmMap->first).outDUs.insert(outerDU);
                dbmMap->operator[](outerDU).inDUs.insert(it_dbmMap->first);
            } else {

                // Warning message
                if (verbose)
                    std::cout << "#WOP> MODULE NOT FOUND! " << it_miWithoutPorts->first->getDesignUnit()->c_str()
                              << " ( " << it_dbmMap->first->getName()->c_str() << " ) " << std::endl;
            }
        }
    }

    if (verbose)
        printInDusOutDus(dbmMap);
}


// For moduleInstWithoutPorts
// Given a ViewReference (e.g. <VIEWREFERENCE name="behav" typeVariant="NATIVE_TYPE" unitName="xor_">)
// return its design unit (e.g. "xor_" design unit)
hif::DesignUnit *findDesignUnitFromViewReference(hif::ViewReference *const viewref,
                                                 std::vector<hif::Object *> *objVector,
                                                 std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // dbmMap iterator
    std::map<hif::DesignUnit *, DBM>::iterator it_dbmMap;

    // Viewreference's DU name
    std::string designUnitName = viewref->getDesignUnit()->c_str();

    int vectorSize = 0;


    // For each dbmMap's element
    for (it_dbmMap = dbmMap->begin(); it_dbmMap != dbmMap->end(); ++it_dbmMap) {

        // If design unit has the same name
        if (designUnitName == getObjName(it_dbmMap->first)) {

            // Counting ports
            for (int i = 0; i < objVector->size(); ++i) {

                // If not i from a[i] or a[i:j]
                if (!isIndex(objVector->operator[](i)))
                    vectorSize++;
            }

            // Checking if it has same dimension
            if (vectorSize == it_dbmMap->second.dus.size())
                return it_dbmMap->first;

        }
        vectorSize = 0;
    }

    return nullptr;
}


// For moduleInstWithPorts
// Given a ViewReference (e.g. <VIEWREFERENCE name="behav" typeVariant="NATIVE_TYPE" unitName="xor_">)
// return its design unit (e.g. "xor_" design unit)
hif::DesignUnit *findDesignUnitFromViewReference(hif::ViewReference *const viewref,
                                                 std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // dbmMap iterator
    std::map<hif::DesignUnit *, DBM>::iterator it_dbmMap;

    // Viewreference's DU name
    std::string designUnitName = viewref->getDesignUnit()->c_str();


    // For each dbmMap's element
    for (it_dbmMap = dbmMap->begin(); it_dbmMap != dbmMap->end(); ++it_dbmMap) {

        // If design unit has the same name
        if (designUnitName == getObjName(it_dbmMap->first)) {
            return it_dbmMap->first;
        }
    }

    return nullptr;
}


// Find paths:
//    - Paths initialization: design unit with outDUs.size() = 0 and inDUs.size() != 0
//          (the design unit doesn't instantiate any module and it's instantiated by at least one module)
//    - Recursion:
//          - findPathsRecursive()
//          - pathsEraseDuplicate()
//
// Note: the path creation starts from the end
//       inDUs, design units calling the current design unit
//       outDUs, design units called by the current design unit
void findPaths(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths,
               std::map<hif::DesignUnit *, DBM> *dbmMap) {

    bool verbose = isVerbose();

    // Paths iterator
    std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>>::iterator it_paths;

    // dbmMap iterator
    std::map<hif::DesignUnit *, DBM>::iterator it_dbmMap;

    // Design unit set iterator
    std::set<hif::DesignUnit *>::iterator it_duSet;


    // First step, starting from the end
    for (it_dbmMap = dbmMap->begin(); it_dbmMap != dbmMap->end(); ++it_dbmMap) {

        // This module doesn't instantiate module and it's not instantiated by other modules
        if (it_dbmMap->second.inDUs.size() == 0 && it_dbmMap->second.outDUs.size() == 0) {
            // Warning message
            if (verbose)
                std::cout << "#> " << getObjName(it_dbmMap->first)
                          << " doesn't belong to any path." << std::endl << std::endl;

        } else if (it_dbmMap->second.outDUs.size() == 0 && it_dbmMap->second.inDUs.size() != 0) {
            // If it's at the end of a path

            for (it_duSet = it_dbmMap->second.inDUs.begin();
                 it_duSet != it_dbmMap->second.inDUs.end(); ++it_duSet) {

                // Inserts {inDu, {inDu, du} }
                paths->insert({it_duSet.operator*(), {it_duSet.operator*(), it_dbmMap->first}});
            }
        }
    }

    // Path creation
    bool doAgain, ret = false;
    do {
        doAgain = false;

        // Paths, map: < "First DU of the path", "path" >
        //      < hif::DesignUnit *, std::vector<hif::DesignUnit *> >
        for (it_paths = paths->begin(); it_paths != paths->end(); ++it_paths) {

            // (du, currentPath, paths, dbmMap)
            ret = findPathsRecursive(it_paths->first, &it_paths->second, paths, dbmMap);

            doAgain = doAgain || ret;
            if (ret)
                it_paths = paths->begin();

        }
        pathsEraseDuplicate(paths);

    } while (doAgain);

    if (verbose)
        printPaths(paths);
}


// Find paths, recursion:
//    - If the inDUs of the current design unit is empty:
//          return
//    - Create a new path for each design unit that instantiates the first design unit of the path.
//    - Erase the "original" path from the paths map
//
// Note: the path creation starts from the end
//       inDUs, design units calling the current design unit
//       outDUs, design units called by the current design unit
bool findPathsRecursive(hif::DesignUnit *du,
                        std::vector<hif::DesignUnit *> *currentPath,
                        std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths,
                        std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // If inDUs empty
    if (dbmMap->operator[](du).inDUs.size() == 0) {
        return false;
    }


    // Design unit set iterator
    std::set<hif::DesignUnit *>::iterator it_duSet;

    // Paths iterator
    std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>>::iterator it_paths;

    // "New" path, insert an empty Design unit in first position
    std::vector<hif::DesignUnit *> vec = *currentPath;
    vec.insert(vec.begin(), nullptr);


    // For each Design Unit in duSet of du
    for (it_duSet = dbmMap->operator[](du).inDUs.begin(); it_duSet != dbmMap->operator[](du).inDUs.end(); ++it_duSet) {

        // Set the first element of the "new" path
        vec[0] = it_duSet.operator*();

        // Add the "new" path (vec)
        paths->insert({it_duSet.operator*(), vec});

        // (du, currentPath, paths, dbmMap)
        findPathsRecursive(it_duSet.operator*(), &vec, paths, dbmMap);
    }

    // Find initial path, erase it.
    // The initial path is "old" because a new paths has been created from it (we started from the end of paths).
    for (it_paths = paths->begin(); it_paths != paths->end(); ++it_paths) {

        if (du == it_paths->first && currentPath == &(it_paths->second)) {
            paths->erase(it_paths);
            break;
        }
    }

    return true;
}


// Erase duplicate paths
void pathsEraseDuplicate(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths) {

    // Paths iterators
    std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>>::iterator it_paths1, it_paths2;

    int i;
    bool toErase = true;


    // For each path
    for (it_paths1 = paths->begin(); it_paths1 != paths->end(); ++it_paths1) {

        // For each path after it_paths1
        for (it_paths2 = it_paths1; it_paths2 != paths->end();) {
            ++it_paths2; // Starting from the next path

            // If the two paths have the same size and the same starting point
            if (it_paths2 != paths->end() &&
                it_paths1->first == it_paths2->first &&
                it_paths1->second.size() == it_paths2->second.size()) {

                // Check if equal
                for (i = 0; i < it_paths1->second.size(); ++i) {

                    // If a difference is found, sets toErase to False
                    if (it_paths1->second[i] != it_paths2->second[i]) {
                        toErase = false;
                        break;
                    }
                }

                // Erasing
                if (toErase) {
                    paths->erase(it_paths1);

                    // Prevent Core dump
                    it_paths1 = paths->begin();
                    it_paths2 = paths->begin();
                    break;
                }
            }

            toErase = true;
        }
    }
}


// For each path, for each pair of design units (starting from the end),
// the previous design units's CDFG inherits the next design units's CDFG and the module linking arches
//
// E.g. Path: module A --> module B --> module C
//          module C's CDFG ==> module B's CDFG and then module B's CDFG ==> module A's CDFG
//      At the end of inheritGraphs(), module A's CDFG =
//          A's CDFG + module linking A-B + B's CDFG + module linking B-C + C's CDFG
void inheritGraphs(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths,
                   std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // Paths iterators
    std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>>::iterator it_paths;

    // dbmMap iterators
    std::map<hif::DesignUnit *, DBM>::iterator it_dbmMap;

    std::vector<hif::DesignUnit *> *path;
    int i;

    // For each path
    for (it_paths = paths->begin(); it_paths != paths->end(); ++it_paths) {
        path = &(it_paths->second);

        // For each pair of DesignUnits
        for (i = path->size() - 1; i > 0; --i) {
            moveGraph(path->operator[](i), path->operator[](i - 1), dbmMap);
        }
    }
}


// Move fromDu's CDFG in toDU's CDFG:
//
// Looking for fromDU instantiation in moduleInstWithPorts and moduleInstWithoutPorts* of toDU
// If found, create objBindVector (a vector that contains: fromDU's port, toDU's parameter and port direction)
//      If it finds an index (e.g. "i" in "a[i]"), it adds the arch "i --> a" to the toDU's CDFG
// Then merge CDFGs through mergeGraph (passing objBindVector)
//
// Note*: Module instances:
//            - moduleInstWithPorts, e.g. "modB B1 (.x(a), .y(b), .z(c))"
//            - moduleInstWithoutPorts, e.g. "modB B2 (a, b, c)"
void moveGraph(hif::DesignUnit *fromDu,
               hif::DesignUnit *toDu,
               std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // <*fromDuObj, *toDuObj, portDirection>
    std::vector<std::tuple<hif::Object *, hif::Object *, hif::PortDirection>> objBindVector;

    // moduleInstWithoutPorts iterator
    std::map<hif::ViewReference *, std::vector<hif::Object *>>::iterator it_miWithoutPorts;

    // moduleInstWithPorts iterators
    std::map<hif::ViewReference *, std::map<hif::PortAssign *, std::vector<hif::Object *>>>::iterator it_miWithPorts;
    std::map<hif::PortAssign *, std::vector<hif::Object *>>::iterator it_miWithPortsSecond;

    hif::Port *port;
    int i, j, k;

    std::string fromDuName = getObjName(fromDu);


    // For each moduleInstWithPorts element
    for (it_miWithPorts = dbmMap->operator[](toDu).moduleInstWithPorts.begin();
         it_miWithPorts != dbmMap->operator[](toDu).moduleInstWithPorts.end(); ++it_miWithPorts) {

        // If same DesignUnit
        if (it_miWithPorts->first->getDesignUnit()->c_str() == fromDuName) {

            for (it_miWithPortsSecond = it_miWithPorts->second.begin();
                 it_miWithPortsSecond != it_miWithPorts->second.end(); ++it_miWithPortsSecond) {

                // For each ports
                for (i = 0; i < it_miWithPortsSecond->second.size(); ++i) {

                    // Check if it's inside brackets: "a[i]" or "a[j:i]"
                    if (isIndex(it_miWithPortsSecond->second.operator[](i))) {

                        // Checks if it's not a value
                        if (!isValue(it_miWithPortsSecond->second.operator[](i))) {
                            // E.g. if "add A0 (...,.x(a[i]), ...)" then add "i --> a"
                            dbmMap->operator[](toDu).cdfg.add(it_miWithPortsSecond->second.operator[](i),
                                                              it_miWithPortsSecond->second.operator[](0));
                        }

                    } else { // Otherwise: e.g. "b" in "add A0 (..., .y(b), ...)" or "a" in "add A0 (..., .x(a[i]), ...)"
                        port = getPortPointer(getObjName(it_miWithPortsSecond->first), fromDu, dbmMap);

                        // If it's a Identifier (not a Value) and the port has been found
                        if (dynamic_cast<hif::Identifier *>(it_miWithPortsSecond->second.operator[](i)) != nullptr &&
                            port != nullptr) {

                            objBindVector.push_back(std::make_tuple(port,
                                                                    it_miWithPortsSecond->second.operator[](i),
                                                                    port->getDirection()));
                        }
                    }
                }
            }

            //Graph merge
            dbmMap->operator[](toDu).cdfg.mergeGraph(&dbmMap->operator[](fromDu).cdfg, &objBindVector);
            objBindVector.clear();
        }
    }


    hif::PortDirection portDir;
    bool portFound = false;

    // For each moduleInstWithoutPorts element
    for (it_miWithoutPorts = dbmMap->operator[](toDu).moduleInstWithoutPorts.begin();
         it_miWithoutPorts != dbmMap->operator[](toDu).moduleInstWithoutPorts.end(); ++it_miWithoutPorts) {

        // If it finds a module instance with same fromDU name
        if (it_miWithoutPorts->first->getDesignUnit()->c_str() == fromDuName) {
            i = 0;
            j = 0;
            k = 0;

            // For each ports
            for (; i < it_miWithoutPorts->second.size(); ++i) {

                // Check if it's inside brackets: "a[i]" or "a[j:i]"
                if (isIndex(it_miWithoutPorts->second.operator[](i))) {
                    // j used to point the fromDu's element: if "add A0 (..., a[i], ...)", both "a" and "i" are saved in miWP
                    --j;

                    // Checks if it's not a value
                    if (!isValue(it_miWithoutPorts->second.operator[](i))) {

                        // If "add A0 (..., a[i], ...)" then add "i --> a"
                        dbmMap->operator[](toDu).cdfg.add(it_miWithoutPorts->second.operator[](i),
                                                          it_miWithoutPorts->second.operator[](k));
                    }


                } else { // Otherwise: "b" in "add A0 (..., b, ...)" or "a" in "add A0 (..., a[i], ...)"

                    // Get PortDirection, boolean tells if it found the direction (PortDirection doens't have "null" value)
                    std::tie(portFound, portDir) = getPortDirection(dbmMap->operator[](fromDu).dus[j],
                                                                    fromDu,
                                                                    dbmMap);

                    // If it's a Identifier (not a Value) and the port has been found
                    if (dynamic_cast<hif::Identifier *>(it_miWithoutPorts->second.operator[](i)) != nullptr &&
                        portFound) {

                        objBindVector.push_back(std::make_tuple(dbmMap->operator[](fromDu).dus[j],
                                                                it_miWithoutPorts->second.operator[](i), portDir));
                    }
                }

                ++j;
            }

            //Graph merge
            dbmMap->operator[](toDu).cdfg.mergeGraph(&dbmMap->operator[](fromDu).cdfg, &objBindVector);
            objBindVector.clear();

            // For futher indexs
            k = i;
        }
    }
}


// Given a dbmMap, returns a vector containing "main" design units
// Main DU: a design unit with indegree = 0 (a module not instantiated by any other module)
std::vector<hif::DesignUnit *> getMainDus(std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // dbmMap iterator
    std::map<hif::DesignUnit *, DBM>::iterator it_dbmMap;

    std::vector<hif::DesignUnit *> mainDuVector;

    for (it_dbmMap = dbmMap->begin(); it_dbmMap != dbmMap->end(); ++it_dbmMap) {
        // If empty inDUs (none module istantiate this DU)
        if (it_dbmMap->second.inDUs.size() == 0)
            mainDuVector.push_back(it_dbmMap->first);
    }

    return mainDuVector;
}


// Return the ports of a DesignUnit:
//   if isInput = True, returns input and inoutput ports,
//   if isInput = False, returns output and inoutput ports.
std::vector<hif::Object *> getDuPorts(hif::DesignUnit *du,
                                      bool isInput, bool isOutput,
                                      std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // Du's (parameter) ports
    std::vector<hif::Port *> ports = dbmMap->operator[](du).dus;

    // Port vector, function return
    std::vector<hif::Object *> portVector;

    hif::PortDirection portDir;
    int i;


    // For each port
    for (i = 0; i < ports.size(); ++i) {
        // Get port direction
        portDir = ports[i]->getDirection();

        if ((isInput && (portDir == hif::dir_inout || portDir == hif::dir_in)) ||
            (isOutput && (portDir == hif::dir_inout || portDir == hif::dir_out))) {

            portVector.push_back(ports[i]);
        }
    }

    return portVector;
}


// Return the hif::Port* of the object, otherwise return a nullptr
hif::Port *getPortPointer(hif::Object *obj,
                          hif::DesignUnit *du,
                          std::map<hif::DesignUnit *, DBM> *dbmMap) {
    return getPortPointer(getObjName(obj), du, dbmMap);
}


// Returns the hif::Port* of the object, otherwise return a nullptr
hif::Port *getPortPointer(std::string objName,
                          hif::DesignUnit *du,
                          std::map<hif::DesignUnit *, DBM> *dbmMap) {

    std::vector<hif::Port *> *ports = &dbmMap->operator[](du).dus;

    // For each port, if same name, return it
    for (int i = 0; i < ports->size(); ++i)
        if (objName == getObjName(ports->operator[](i)))
            return ports->operator[](i);

    return nullptr;
}


// Returns a pair:
//    - True if the port is found, otherwise False
//    - port direction (if the port was found)
std::pair<bool, hif::PortDirection> getPortDirection(hif::Object *obj,
                                                     hif::DesignUnit *du,
                                                     std::map<hif::DesignUnit *, DBM> *dbmMap) {

    std::vector<hif::Port *> *ports = &dbmMap->operator[](du).dus;

    std::string objName = getObjName(obj);

    // For each port, if same name, return portDirection
    for (int i = 0; i < ports->size(); ++i)
        if (objName == getObjName(ports->operator[](i)))
            return {true, ports->operator[](i)->getDirection()};

    return {false, hif::dir_in};
}


// Print the #inDUs and the #outDUs of each design unit inside the dbmMap
//
// Note: inDUs, design units calling the current design unit
//       outDUs, design units called by the current design unit
void printInDusOutDus(std::map<hif::DesignUnit *, DBM> *dbmMap) {
    // Heading
    std::cout << std::endl << " ==== [IN DU / OUT DU] ==== " << std::endl << std::endl;

    // For each module
    for (auto iter = dbmMap->begin(); iter != dbmMap->end(); ++iter) {
        std::cout << "\t" << iter->second.inDUs.size() << " / " << iter->second.outDUs.size()
                  << "\t" << getObjName(iter->first) << std::endl;
    }

    // Separator
    std::cout << std::endl << "################################################" << std::endl << std::endl;
}


// Print each path inside the paths vector
void printPaths(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths) {
    // Heading
    std::cout << " ==== [PATHS] ==== " << std::endl << std::endl;

    // For each path
    for (auto iter = paths->begin(); iter != paths->end(); ++iter) {

        for (int i = 0; i < iter->second.size(); ++i) {
            std::cout << "\t| " << getObjName(iter->second.operator[](i));
        }
        std::cout << std::endl;
    }

    // Separator
    std::cout << std::endl << "################################################" << std::endl << std::endl;
}


// Print design unit's info (CDFG size, moduleInstWithPorts size, moduleInstWithoutPorts size)
void printDuInfo(hif::DesignUnit *du,
                 std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // Heading
    std::cout << " ==== [MODULE - " << getObjName(du) << "] ==== " << std::endl;

    // Design unit info
    std::cout << "\tSize cdfg: " << dbmMap->operator[](du).cdfg.size() << std::endl;
    std::cout << "\tSize with: " << dbmMap->operator[](du).moduleInstWithPorts.size() << std::endl;
    std::cout << "\tSize without: " << dbmMap->operator[](du).moduleInstWithoutPorts.size() << std::endl;

    // Separator
    std::cout << std::endl << "################################################" << std::endl << std::endl;
}


// Print Design unit's module linking (ports, module call with name, module call without name)
void printModuleLinking(hif::DesignUnit *du,
                        std::map<hif::DesignUnit *, DBM> *dbmMap) {

    // moduleInstWithPorts iterators
    std::map<hif::ViewReference *, std::map<hif::PortAssign *, std::vector<hif::Object *>>>::iterator it_moduleInstWithPorts;
    std::map<hif::PortAssign *, std::vector<hif::Object *>>::iterator it_moduleInstWithPorts_internal;

    // moduleInstWithoutPorts iterator
    std::map<hif::ViewReference *, std::vector<hif::Object *>>::iterator it_moduleInstWithoutPorts;


    // Heading
    std::cout << " #### [MODULE: " << getObjName(du) << "] #### " << std::endl << std::endl;

    if (dbmMap->find(du) == dbmMap->end()) {
        std::cout << " !!!! [MODULE NOT FOUND] !!!! " << std::endl << std::endl;

    } else {
        // Print map of [hif::DesignUnit - hif::Port]
        std::cout << " ==== [MODULE - PORTS] ==== " << std::endl;

        std::cout << " \t[ " << getObjName(du) << " \t --> ";

        // For each port
        for (int i = 0; i < dbmMap->operator[](du).dus.size(); ++i) {
            std::cout << getObjName(dbmMap->operator[](du).dus[i]) << " ";
        }
        std::cout << "]" << std::endl << std::endl;


        // Print map of [hif::PortAssign - hif::Identifier]
        std::cout << " ==== MODULE CALL - PASSED VARIABLES WITH PORT NAME ==== " << std::endl;

        // For each module instance (moduleInstWithPorts)
        for (it_moduleInstWithPorts = dbmMap->operator[](du).moduleInstWithPorts.begin();
             it_moduleInstWithPorts != dbmMap->operator[](du).moduleInstWithPorts.end(); ++it_moduleInstWithPorts) {

            std::map<hif::PortAssign *, std::vector<hif::Object *>> internalMap = it_moduleInstWithPorts->second;

            std::cout << " \t[ " << it_moduleInstWithPorts->first->getDesignUnit()->getString() << " \t --> ";

            for (it_moduleInstWithPorts_internal = internalMap.begin();
                 it_moduleInstWithPorts_internal != internalMap.end(); ++it_moduleInstWithPorts_internal) {

                for (int i = 0; i < it_moduleInstWithPorts_internal->second.size(); ++i) {
                    std::cout << "(" << it_moduleInstWithPorts_internal->first->getName()->getString()
                              << "/" << getObjString(it_moduleInstWithPorts_internal->second.operator[](i)) << ") ";
                }
            }
            std::cout << "]" << std::endl;
        }
        std::cout << std::endl;


        // Print map of [hif::ViewReference - hif::Identifier]
        std::cout << " ==== MODULE CALL - PASSED VARIABLES WITHOUT PORT NAME ==== " << std::endl;

        // For each module instance (moduleInstWithoutPorts)
        for (it_moduleInstWithoutPorts = dbmMap->operator[](du).moduleInstWithoutPorts.begin();
             it_moduleInstWithoutPorts !=
             dbmMap->operator[](du).moduleInstWithoutPorts.end(); ++it_moduleInstWithoutPorts) {

            std::cout << " \t[ " << it_moduleInstWithoutPorts->first->getDesignUnit()->getString() << " \t --> ";

            for (auto &port: it_moduleInstWithoutPorts->second) {
                // Inside a portassign tag can be found identifier, bit, int, char...
                std::cout << getObjString(port) << " ";
            }
            std::cout << "]" << std::endl;
        }

        // Separator
        std::cout << std::endl << "################################################" << std::endl << std::endl;
    }
}


// Print CDFG of the design units inside duVector
void printCDFGs(std::vector<hif::DesignUnit *> *duVector,
                std::map<hif::DesignUnit *, DBM> *dbmMap,
                std::string heading) {

    // Heading
    std::string tempStr = heading + "\n";

    // For each design unit
    for (int i = 0; i < duVector->size(); ++i) {

        // Subheading
        tempStr += "\nModule " + getObjName(duVector->operator[](i)) + ": \n";
        std::cout << tempStr;

        // CDFG
        dbmMap->operator[](duVector->operator[](i)).cdfg.printGraph();

        tempStr = "\n";

        // Separator
        if (i + 1 < duVector->size())
            tempStr += "------------------------------------------------\n";
    }
    std::cout << tempStr;
}
