#include <ast2ddgVisitor.hpp>
#include <ast2cfgVisitor.hpp>
#include <ast2moduleLinking.hpp>
#include <utilsDbmBmatrix.hpp>
#include <CoverageFileParser.hpp>
#include <AssertionFilesParser.hpp>
#include "parseLine.hpp"

#include <chrono>
#include <bits/stdc++.h>
#include <iostream>
#include <unordered_map>


int main(int argc, char *argv[]) {

    hif::applicationUtils::initializeLogHeader("approximation", "");

    // Check the license.
    HIF_LICENSE_CHECK("hif_tools")

    // Get command line options and arguments.
    ParseLine parseLine(argc, argv);

    // Set verbose.
    hif::applicationUtils::setVerboseLog(parseLine.isVerbose());

    // Checks if hif.xml file is in a correct location (e.g. .../approximation/tests/*myProjName*/myFile.hif.xml)
    checkHifXmlPath(argv[argc-1]);

    // Get the input file.
    const std::string inputFile = argv[argc-1];

    // Get test directory path
    const std::string testDirPath = getTestPath(argv[argc-1]);

    // Get project directory path
    const std::string projDirPath = getProjPathFromTestPath(testDirPath);


    // Statement coverage map
    // <filename, <lineNumber, coverageHit>>
    std::unordered_map<std::string, std::unordered_map<size_t, size_t>> statementCoverageMap;

    // Branch coverage map
    // <filename, <lineNumber, (conditionHit, totalConditionHit, itemNumber)>>
    std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> branchCoverageMap;

    // Get coverage maps
    CoverageFileParser cfp(projDirPath, &statementCoverageMap, &branchCoverageMap);
    cfp.setStatementCoverageMap();
    cfp.setBranchCoverageMap();


    
    // Assertion vector: for each assertion save the depth of each identifier in the consequent
    // map<identifier name, identifier depth>
    std::vector<std::map<std::string, size_t>> assertionDepths;

    // Assertion contingency vector: for each assertion save all and TrueTrue instances
    // vector<pair{all instances, True antecedent-True consequent instances}>
    std::vector<std::pair<size_t, size_t>> assertionContingency;

    AssertionDepthsFileParser assertionDepthsFileParser(projDirPath, &assertionDepths, &assertionContingency);
    assertionDepthsFileParser.setAssertionDepths();
    assertionDepthsFileParser.setAssertionContingency();



    // Read the system description.
    hif::System *system = dynamic_cast<hif::System *>(hif::readFile(inputFile));

    // Check the parsing.
    if (system == nullptr)
        messageError("File : " + inputFile + "\n\t\tWrong hif.xml system description.", nullptr, nullptr);


    std::map<hif::DesignUnit *, DBM> dbmMap;

    // Streams
    std::ofstream file;
    std::streambuf *sb;

    // If true, B matrix won't have distance
    short int orderMode;

    auto timeBegin = std::chrono::high_resolution_clock::now();



    // Fill dependencies between modules (DBM)
    ast2moduleLinking visitorDbm(&dbmMap);
    visitorDbm.visitSystem(*system);

    // Fill control flow graph (CFG)
    ast2cfgVisitor visitorCFG(&dbmMap);
    visitorCFG.visitSystem(*system);

    // Fill CDFG with data dependencies graph (DDG)
    ast2ddgVisitor visitorDDG(&dbmMap);
    visitorDDG.visitSystem(*system);

    // Setting inDUs and outDUs for each DBM
    setInDusOutDus(&dbmMap);

    // Find all paths
    std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> paths;
    findPaths(&paths, &dbmMap);

    // For every path, update graphs
    inheritGraphs(&paths, &dbmMap);

    // Main DUs (inDU empty)
    std::vector<hif::DesignUnit *> mainDuVector = getMainDus(&dbmMap);

    auto timeEnd = std::chrono::high_resolution_clock::now();

    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(timeEnd - timeBegin).count();
    std::cout << std::endl << " ==== [CDFG GENERATED, EXECUTION TIME: " << duration << "ms] ==== " << std::endl
              << std::endl;



    // Std::cout redirect
    file.open(testDirPath + "CDFG.txt");
    sb = setCout(&file);

    // Prints CDFG
    printCDFGs(&mainDuVector, &dbmMap, " ==== [MAIN GRAPHS] ==== ");

    // Std::cout restore
    setCout(sb);
    file.close();



    // Bmatrix
    getBmatrixs(&dbmMap, mainDuVector, system,
                &statementCoverageMap, &branchCoverageMap, &assertionDepths, &assertionContingency,
                testDirPath, projDirPath);

    return 0;
}
