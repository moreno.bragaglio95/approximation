#include "AssertionFilesParser.hpp"

// Constructor
AssertionDepthsFileParser::AssertionDepthsFileParser(std::string projDirPath,
                                                     std::vector<std::map<std::string, size_t>> *assertionsDepths,
                                                     std::vector<std::pair<size_t, size_t>> *assertionContingency) {

    this->projDirPath = projDirPath;
    this->assertionDepths = assertionsDepths;
    this->assertionContingency = assertionContingency;
}


// This code parses a file containing information about assertions' consequent.
// This program assumes that the structure of the branch coverage file is exactly the same of the next structure example.
//
// request 4 ; byte_sel 5 data_from_bus 5
// busy 4 ; byte_sel 5 data_from_bus 5
// wb_stall 4 ; byte_sel 5 data_from_bus 5
// request 5 busy 4 ; byte_sel 5 address 5
// wb_ack 2 ; byte_sel 4

// Fill assertionDepths set
void AssertionDepthsFileParser::setAssertionDepths() {

    std::string line, subline, id;
    size_t depth;

    // Antecedent-Consequent delimiter
    std::string delim = " ; ";

    // Open assertionDepths file
    std::ifstream ifs(projDirPath + assertionDepthsFileName);
    if (ifs.fail()) {
        std::cout << "#> " << assertionDepthsFileName << " not found." << std::endl;
        return;
    }


    std::map<std::string, size_t> *lastAssertionMap; // Abbreviation

    // For each line
    while (getline(ifs, line)) {
        if (line.empty()) // If empty, skip
            continue;

        assertionDepths->push_back({}); // New assertion

        // E.g. "request 4 ; byte_sel 5 data_from_bus 5"
        if (line.find(delim) != std::string::npos) {

            // E.g. "byte_sel 5 data_from_bus 5"
            std::stringstream cons(line.substr(line.find(delim) + 3)); // +3 because " ; "
            // E.g. "byte_sel" "5" "data_from_bus" "5"
            while (getline(cons, subline, ' ')) {
                id = subline;
                getline(cons, subline, ' ');
                depth = std::stoi(subline);

                lastAssertionMap = &assertionDepths->operator[](assertionDepths->size() - 1);

                if (lastAssertionMap->find(id) == lastAssertionMap->end() ||
                    lastAssertionMap->operator[](id) > depth) { // If it's a new id or the current depth is minor

                    lastAssertionMap->operator[](id) = depth;
                }
            }

        }
    }
}


// Fill assertion contingency vector
void AssertionDepthsFileParser::setAssertionContingency() {

    std::string line, subline;
    size_t instancesTT, instacesAll;

    // Value delimiter
    std::string delim = " ";

    // Open assertionContingency file
    std::ifstream ifs(projDirPath + assertionContingencyFileName);
    if (ifs.fail()) {
        std::cout << "#> " << assertionContingencyFileName << " not found." << std::endl;
        return;
    }

    // For each line
    while (getline(ifs, line)) {
        if (line.empty()) // If empty, skip
            continue;

        instancesTT = -1;
        instacesAll = 0;

        std::stringstream lineSS(line);
        while (getline(lineSS, subline, ' ')) {
            if (instancesTT == -1)
                instancesTT = std::stoi(subline);

            instacesAll += std::stoi(subline);
        }

        assertionContingency->push_back({instacesAll, instancesTT});
    }
}
