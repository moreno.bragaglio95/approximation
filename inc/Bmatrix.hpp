#pragma once

#include "utilsDbm.hpp"
#include <unordered_map>
#include <bits/stdc++.h>
#include <iomanip>


//    Tuple:
//        - statement,
//        - line number,
//        - statement string,
//        - dependencies,
//        - coverage counter
//        - impact on assertion set
typedef std::tuple<hif::Object *, size_t, std::string, std::vector<dependency>, size_t, float> bmTuple;


// B matrix class
class Bmatrix {

public:
    // Constructor
    Bmatrix(std::vector<hif::Object *> columns,     // Output
            short int orderMode,                    // Order mode
            std::string testDirPath,                // Test directory path
            hif::DesignUnit *rootDu,                // Root design unit
            std::string fileCounter);               // File counter (e.g "003" in "file_1_003_tb.txt")

    // Constructor
    Bmatrix(std::vector<hif::Object *> columns,     // Output
            short int orderMode,                    // Order mode
            std::string testDirPath,                // Test directory path
            hif::DesignUnit *rootDu,                // Root design unit
            std::string fileCounter,                // File counter (e.g "003" in "file_1_003_tb.txt")
            std::vector<bmTuple> bmatrix);          // B matrix

    // Return the column vector
    std::vector<hif::Object *> *getColumns();

    // Add a row into the matrix
    void add(hif::Object *statementObj,
             size_t lineNumber,
             std::string statementString,
             std::vector<dependency> dependencies,
             size_t coverageCounter,
             float assertionImpact);

    // Print B matrix
    void printBmatrix(hif::DesignUnit *du);

    // Given an output, return its position (-1 if not found)
    int getOutputPosition(hif::Object *obj);

    // Given a statement object, return the row position (-1 if not found)
    int getObjRowPosition(hif::Object *obj);

    // Return the output vector
    std::vector<hif::Object *> getOutputs();

    // Return the unordered B matrix
    std::vector<bmTuple> getUnorderedBmatrix();


private:
    // Print rows, orderMode = 1
    void printRows_1(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                     int paddingMax);

    // Print rows, orderMode = 2 or 3
    void printRows_23(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                      int paddingMax);

    // Print rows, orderMode = 4
    void printRows_4(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                     int paddingMax);

    // Print a single row of the B matrix
    void printRow(bmTuple *tuple,
                  float pr,
                  int paddingMax);

    // Return the priority of the row
    float getRowPriority(std::vector<dependency> deps,
                         std::vector<std::pair<float, hif::Object *>> *outputPriority);

    // Return the priority of the row, Influences
    // More 1s --> higher priority
    float getPr1_InfluencedOutputsNumber(std::vector<dependency> deps);

    // Return the priority of the row, Average
    // If a col is 0 --> distance = [(maxDistance * 2) + 1]
    float getPr2_DistancesAverage(std::vector<dependency> deps);

    // Return the priority of the row, Weighted average
    // If a col is 0 --> distance = {[(maxDistance * 2) + 1] * peso}
    float getPr3_DistancesWeightedAverage(std::vector<dependency> deps,
                                          std::vector<std::pair<float, hif::Object *>> *outputWeight);

    // Print outputs as a list
    void printObjectVector(std::vector<hif::Object *> *objs,
                           bool isBulletedList,
                           bool isNumberedList);

    // Return the minimum dependency of the n-th output (n = outputPos) from a specific bmatrix_red
    std::pair<bool, int> getMinimumDependency( std::vector<std::tuple<hif::Object *, std::vector<dependency>, float>> *bmatrix_red_tmp,
                                               int position);

    // Set weight of each outputs
    void setOutputsWeight(std::vector<std::pair<float, hif::Object *>> *outputWeight,
                          hif::DesignUnit *du);

    // Set the importance of the outputs. It fills outputImportance.
    void setOutputsImportance(std::vector<std::pair<float, hif::Object *>> *outputPriority,
                              hif::DesignUnit *du);

    // Order the B matrix based on output importance
    void orderBmatrixByOutputImportance(
            std::vector<std::tuple<hif::Object *, std::vector<dependency>, float>> *bmatrix_red,
            std::vector<std::pair<float, hif::Object *>> *outputPriority);

    // Set maxDistance (the maximum dependency distance in the B matrix)
    void setMaxDistance();

    // Set maxImpact (the maximum impact in the B matrix)
    void setMaxImpact();


    // Outputs
    std::vector<hif::Object *> columns;

    // #outputs
    unsigned short int columnNumber;

    // Bmatrix order mode
    short int orderMode;

    // Path where save files
    std::string testDirPath;

    // Root design unit
    hif::DesignUnit *rootDu;

    // File counter (eg. Bmatrix_027_1.txt: fileCounter="027", orderMode=1)
    std::string fileCounter;

    // Bmatrix
    std::vector<bmTuple> bmatrix;

    // Maximum distance of Bmatrix
    unsigned int maxDistance;

    // Maximum impact
    float maxImpact;

};
