/*
 * This class is used in order to visit the XML through the hif::GuideVisitor class and generate the concerned Dependencies Between Modules (DBM)
 * INPUT = xml file generated with verilog2ast command
 * OUTPUT = DBM
 *
 * General workflow:
 * Every module generate a DBM but during the visiting I will catch the linking between module call and the considered variables.
 *    At the end of all module visits a renaming will be done.
 *      example:
 *          in module_main there is a statement:            module_and(A,B,C);
 *          in module_and(X,Y,Z) there is a statement:      X = Y & Z;
 *          then the DBM for module_and will be generated:
 *                  z <-- X --> Y
 *          and at the end of all visiting a renaming will e performed (X takes A, Y takes B, Z takes C)
 *                  C <-- A --> B
 * */

#pragma once

#include <hif/GuideVisitor.hh>
#include "Graph.hpp"
#include "utilsDbm.hpp"

#include <future>
#include <memory>


class ast2moduleLinking : public hif::GuideVisitor {

public:
    // Constructors
    ast2moduleLinking();

    ast2moduleLinking(std::map<hif::DesignUnit *, DBM> *dbmStruct);


    // Visitors
    int visitLibraryDef(hif::LibraryDef &o) override;

    int visitDesignUnit(hif::DesignUnit &o) override;

    int visitIdentifier(hif::Identifier &o) override;

    int visitPortAssign(hif::PortAssign &o) override;

    int visitPort(hif::Port &o) override;

    int visitViewReference(hif::ViewReference &o) override;

    bool BeforeVisit(hif::Object &o) override;

    int AfterVisit(hif::Object &o) override;


    // Print module linking data
    void printMap();


private:
    // Save the value of the object
    void saveValues(hif::Object *o);

    // Save the Design Unit port
    void saveDU_Ports(hif::Port *o);

    // Save the Port identifier
    void savePort_Ident(hif::Object *o);

    // Save the Viewreference identifier
    void saveViewref_Ident(hif::Object *o);


    std::map<hif::DesignUnit *, DBM> *dbmStruct;

    hif::DesignUnit *lastVisitedDU = nullptr;
    hif::ViewReference *lastVisitedViewRef = nullptr;
    hif::PortAssign *lastVisitedPortAssign = nullptr;
    bool portAssignVisited = false;

    // Save Design units with concerned ports
    std::vector<hif::Port *> dus;
    std::vector<hif::Port *>::const_iterator it_dus;

    // Save viewreference with concerned portassign/identifier (viewreference is an XML tags used for designunit instances)
    std::map<hif::ViewReference *, std::map<hif::PortAssign *, std::vector<hif::Object *>>> moduleInstWithPorts;
    std::map<hif::ViewReference *, std::map<hif::PortAssign *, std::vector<hif::Object *>>>::iterator it_moduleInstWithPorts;
    std::map<hif::PortAssign *, std::vector<hif::Object *>> moduleInstWithPorts_internal;
    std::map<hif::PortAssign *, std::vector<hif::Object *>>::iterator it_moduleInstWithPorts_internal;
    /*
     * Filled (visiting the identifier) if the portassign xml tag has a name (so I already know the link between
     * module call and its linked variables).
     * In this case the code is like this:
     *
     * <PORTASSIGN direction="NONE" name="a">
     *     <VALUE>
     *          <IDENTIFIER name="p2_p0">
     *              <CODE_INFO column_number="29" file="sobel.v" line_number="26"/>
     *          </IDENTIFIER>
     *     </VALUE>
     *      <CODE_INFO column_number="29" file="sobel.v" line_number="26"/>
     * </PORTASSIGN>
     *
     * It means that the verilog instruction is "moduleName(.a(p2_p0))" namely there is an assignment to the port p2_p0
     * through the variable "a" of the module moduleName
     */

    // Save module calls like moduleName(param1, param2, param3, ...). At the end of the visitor, moduleInstWithoutPorts
    // will be read to add link between module decribed by ViewReference and the identifier
    std::map<hif::ViewReference *, std::vector<hif::Object *>> moduleInstWithoutPorts;
    std::map<hif::ViewReference *, std::vector<hif::Object *>>::iterator it_moduleInstWithoutPorts;
    /*
     * Filled (visiting the identifier) if the portassign xml tag has no a name (so I do not know the link between
     * module call and its linked variables).
     * In this case the code is like this:
     *
     * <PORTASSIGN direction="NONE" name="(no name)">
     *     <VALUE>
     *          <IDENTIFIER name="p2_p0">
     *              <CODE_INFO column_number="29" file="sobel.v" line_number="26"/>
     *          </IDENTIFIER>
     *     </VALUE>
     *      <CODE_INFO column_number="29" file="sobel.v" line_number="26"/>
     * </PORTASSIGN>
     *
     * It means that the verilog instruction is "moduleName(a)" namely there is no an assignment to ports but it is
     * passed "a" as argument to the module.
     * I need to save the module name exploiting the ViewReference tag and the related variables (identifiers) and
     * in the future I will go to handle these info while generating the dbm.
     */
};
