#pragma once

#include "utils.hpp"
#include "Graph.hpp"

#include <map>
#include <set>


// <haveDependency, shortest path length>
typedef std::pair<bool, u_int> dependency;

// Data between modules
struct DBM {
    // Grafo
    Graph cdfg;

    // Ports
    std::vector<hif::Port *> dus;
    // Module instances, e.g. "modB B1 (.x(a), .y(b), .z(c))"
    std::map<hif::ViewReference *, std::map<hif::PortAssign *, std::vector<hif::Object *>>> moduleInstWithPorts;
    // Module instances, e.g. "modB B2 (a, b, c)"
    std::map<hif::ViewReference *, std::vector<hif::Object *>> moduleInstWithoutPorts;

    // Design units calling this one
    std::set<hif::DesignUnit *> inDUs;
    // Design units called by this one
    std::set<hif::DesignUnit *> outDUs;
};


// Given dbmMap, sets inDUs and outDus for each design unit
void setInDusOutDus(std::map<hif::DesignUnit *, DBM> *dbmMap);

// For moduleInstWithoutPorts
// Given a ViewReference, return its design unit
hif::DesignUnit *findDesignUnitFromViewReference(hif::ViewReference *const viewref,
                                                 std::vector<hif::Object *> *objVector,
                                                 std::map<hif::DesignUnit *, DBM> *dbmMap);

// For moduleInstWithPorts
// Given a ViewReference, return its design unit
hif::DesignUnit *findDesignUnitFromViewReference(hif::ViewReference *const viewref,
                                                 std::map<hif::DesignUnit *, DBM> *dbmMap);


// Find paths
void findPaths(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths,
               std::map<hif::DesignUnit *, DBM> *dbmMap);

// Find paths, recursion
bool findPathsRecursive(hif::DesignUnit *du,
                        std::vector<hif::DesignUnit *> *currentPath,
                        std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths,
                        std::map<hif::DesignUnit *, DBM> *dbmMap);

// Erase duplicate paths
void pathsEraseDuplicate(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths);


// For each path, for each pair of design units (starting from the end),
// the previous design units's CDFG inherits the next design units's CDFG and the module linking arches
void inheritGraphs(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths,
                   std::map<hif::DesignUnit *, DBM> *dbmMap);

// Move fromDu's graph in toDU's graph
void moveGraph(hif::DesignUnit *fromDu,
               hif::DesignUnit *toDu,
               std::map<hif::DesignUnit *, DBM> *dbmMap);

// Given a dbmMap, returns a vector containing "main" dus (du with indegree = 0)
std::vector<hif::DesignUnit *> getMainDus(std::map<hif::DesignUnit *, DBM> *dbmMap);


// Return the ports of a DesignUnit:
//   if isInput = True, returns input and inoutput ports,
//   if isInput = True, returns output and inoutput ports.
std::vector<hif::Object *> getDuPorts(hif::DesignUnit *du,
                                      bool isInput, bool isOutput,
                                      std::map<hif::DesignUnit *, DBM> *dbmMap);

// Return the hif::Port* of the object, otherwise return a nullptr
hif::Port *getPortPointer(hif::Object *obj,
                          hif::DesignUnit *du,
                          std::map<hif::DesignUnit *, DBM> *dbmMap);

// Return the hif::Port* of the object, otherwise return a nullptr
hif::Port *getPortPointer(std::string objName,
                          hif::DesignUnit *du,
                          std::map<hif::DesignUnit *, DBM> *dbmMap);

// Return a pair:
//    True if the port is found, otherwise False
//    (if True) port direction
std::pair<bool, hif::PortDirection> getPortDirection(hif::Object *o,
                                                     hif::DesignUnit *du,
                                                     std::map<hif::DesignUnit *, DBM> *dbmMap);


// Print inDUs and outDUs
void printInDusOutDus(std::map<hif::DesignUnit *, DBM> *dbmMap);

// Print paths
void printPaths(std::multimap<hif::DesignUnit *, std::vector<hif::DesignUnit *>> *paths);

// Print DesignUnit's info
void printDuInfo(hif::DesignUnit *du,
                 std::map<hif::DesignUnit *, DBM> *dbmMap);

// Print DesignUnit's module linking
void printModuleLinking(hif::DesignUnit *du,
                        std::map<hif::DesignUnit *, DBM> *dbmMap);

// Print the CDFGs of a vector of design units
void printCDFGs(std::vector<hif::DesignUnit *> *duVector,
                std::map<hif::DesignUnit *, DBM> *dbmMap,
                std::string heading);
