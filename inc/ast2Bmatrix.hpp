#pragma once

#include <hifCore.hh>
#include "Graph.hpp"
#include "Bmatrix.hpp"

#include <fstream>
#include <stack>
#include <regex>


class ast2Bmatrix : public hif::GuideVisitor {

public:
    // Constructor
    ast2Bmatrix(Bmatrix *bmatrix,
                Graph *graph,
                std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap,
                std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap,
                std::vector<std::map<std::string, size_t>> *assertionDepths,
                std::vector<std::pair<size_t, size_t>> *assertionContingency,
                std::string projDirPath);

    // Visitors
    int visitLibraryDef(hif::LibraryDef &o) override;

    int visitIdentifier(hif::Identifier &o) override;

    int visitSignal(hif::Signal &o) override;

    bool BeforeVisit(hif::Object &o) override;

    int AfterVisit(hif::Object &o) override;


private:
    // Given an object, return the dependency vector
    std::vector<dependency> getDeps(hif::Object *obj);

    // Given an object, return the impact
    float getObjImpact(hif::Object *obj);

    // Given two dependencies vector (depsA and depsB), save the minimum dependencies vector in depsA
    void getMinDependencies(std::vector<dependency> *depsA, std::vector<dependency> *depsB);

    // Get the line code of the object
    std::string getLine(hif::Object *obj, bool trimIt);

    // Get the coverage hit of the object
    size_t getCoverage(hif::Object *obj);

    // Given a dependencies vector, return the impact (of the statement) on the assertion set
    float getImpact(std::vector<dependency> deps);

    // The B matrix
    Bmatrix *bmatrix;

    // CDFG
    Graph *graph;

    // Statement coverage map:
    // unord_map< filename, unord_map< line number, # statement hit > >
    std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap;

    // Branch coverage map
    // unord_map< filename, unord_map< line number, tuple(
    //                                                  # condition hit (only a specific condition),
    //                                                  # total condition hit (whole condition block),
    //                                                  item number (=n, n-th condition of the line),
    //                                                  classId (it specifies the class id: "if" or "switch-case")
    //                                                  ) > >
    // E.g.
    // 1.  if(a < 10)                   <-- hit = 20 (= total condition hit = first condition hit)
    // 2.      out = 1;                 <-- hit = 4  (e.g. the first condition is evaluated True four times)
    // 3.  else if(10 <= a && a < 20)   <-- hit = 16 (= 20 - 4 = previuos condition hit - previuos condition True hit)
    // 4.      out = 2;                 <-- hit = 6  (e.g. the second condition is evaluated True eight times)
    // 5.  else ...                     <-- hit = 10
    std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap;

    // Assertion depth vector: for each assertion save identifier-depth pair
    // map<identifier string, identifier depth>
    std::vector<std::map<std::string, size_t>> *assertionDepths;

    // Assertion contingency vector: for each assertion save all and TrueAnt-TrueCons instances
    // vector<pair{all instances, True antecedent-True consequent instances}>
    std::vector<std::pair<size_t, size_t>> *assertionContingency;


    // Path of the project directory
    std::string projDirPath;

    // Last visited signal
    hif::Object *lastVisitedSignal = nullptr;

    // Fixs <FORGENERATE> and <IFGENERATE>
    int genLineNumber = 0;
    std::string genSourceFile;

    // Condition's dependencies (for multiple condition)
    std::stack<std::pair<hif::Object *, std::vector<dependency> >> conditionDeps;

    // Assign's dependencies (for a single assign)
    std::vector<dependency> assignDeps;

    bool verbose;
};
