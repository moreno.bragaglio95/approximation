#pragma once

#include <hifCore.hh>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <string>
#include <regex>
#include <unordered_map>


// Coverage file parser class
class CoverageFileParser {

public:
    // Constructor
    CoverageFileParser(std::string projDirPath,
                       std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap,
                       std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap);

    // Fill statement coverage map
    void setStatementCoverageMap();

    // Fill branch coverage map
    void setBranchCoverageMap();


private:
    // Given a coverage condition string (e.g. "IF", "CASE"), returns the hif::ClassId
    hif::ClassId getClassIdFromCoverageString(std::string classString);


    // Statement coverage map:
    // unord_map< filename, unord_map< line number, # statement hit > >
    std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap;

    // Branch coverage map:
    // unord_map< filename, unord_map< line number, tuple(
    //     # condition hit (only a specific condition),
    //     # total condition hit (whole condition block),
    //     item number (=n, n-th condition of the line),
    //     classId (it specifies the class id of the condition: "if" or "switch-case")
    //     ) > >
    //
    // E.g.
    // 1.  if(a < 10)                   <-- hit = 20 (= total condition hit = first condition hit)
    // 2.      out = 1;                 <-- hit = 4  (e.g. the first condition is evaluated True four times)
    // 3.  else if(10 <= a && a < 20)   <-- hit = 16 (= 20 - 4 = previuos condition hit - previuos condition True hit)
    // 4.      out = 2;                 <-- hit = 6  (e.g. the second condition is evaluated True six times)
    // 5.  else ...                     <-- hit = 10
    std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap;

    // Project directory path
    std::string projDirPath;

    // Coverage file name
    std::string coverageFileName = "specific_coverage.txt";
};
