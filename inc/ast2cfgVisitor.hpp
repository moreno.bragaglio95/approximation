/*
 * This class is used in order to visit the XML through the hif::GuideVisitor class and generate the concerned Conditional Flow Graph (CFG)
 * INPUT = xml file generated with "verilog2ast --output nameOfDesideredXml --printonly listOfVerilogFile.v" command
 * OUTPUT = cfgMap concerning:
 *                key: variables of a condition (of if/switch/for/while/sensitivity/...)
 *                value: variables in the corresponding body + variables of the previous condition
 *
 * example:
 *      if(a>b)
 *           c++
 *           if(d>e)
 *               k++
 *           f++
 *      then the corresponding dependence of the first if is: {key:(a,b), value:(c,d,e,f)}
 * */

#pragma once

#include <hifCore.hh>
#include "Graph.hpp"
#include "utilsDbm.hpp"

#include <search.h>
#include <stack>


class ast2cfgVisitor : public hif::GuideVisitor, public hif::BList<hif::Object> {

public:
    // Constructors
    ast2cfgVisitor();

    ast2cfgVisitor(std::map<hif::DesignUnit *, DBM> *dbmMap);


    // Visitors
    int visitLibraryDef(hif::LibraryDef &o) override;

    int visitDesignUnit(hif::DesignUnit &o) override;

    int visitIdentifier(hif::Identifier &o) override;

    bool BeforeVisit(hif::Object &o) override;

    int AfterVisit(hif::Object &o) override;

    // Prints CFG
    void printCFG();


private:
    // Add CFG to CDFG
    void addCFGtoCDFG();

    // Add current Always block to CDFG
    void addAlwaysStackToCDFG();


    // DBM map (Data Between Modules)
    std::map<hif::DesignUnit *, DBM> *dbmMap;

    // Last visited design unit
    hif::DesignUnit *lastVisitedDU = nullptr;

    // Stack to keep trace of scopes during the visiting
    std::stack<std::set<hif::Object *>> condStack;

    // cfgMap <condition_variables, body_variables>
    std::map<std::set<hif::Object *>, std::set<hif::Object *>> cfgMap;

    // cfgMap iterator
    std::map<std::set<hif::Object *>, std::set<hif::Object *>>::iterator it_cfgMap;

    // Stack to keep trace of Always condition
    std::stack<std::tuple<std::vector<hif::Object *>,   // Sensitivity list
            std::vector<hif::Object *>,                 // Influenced objects
            bool,                                       // Has sensitivity list
            size_t> > alwaysStack;                      // Stack size during tuple creation

    // Vector to keep trace of Always sensitivity list
    std::vector<hif::Object *> sensitivityList;

    bool verbose;
};
