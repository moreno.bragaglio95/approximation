#pragma once

#include <hifCore.hh>
#include "utils.hpp"

class ParseLine :
        public hif::applicationUtils::CommandLineParser {
public:

    ParseLine(int argc, char *argv[]);

    ~ParseLine() override = default;

    ParseLine(const ParseLine &) = delete;

    ParseLine &operator=(const ParseLine &) = delete;

    uint64_t getUintProva() const;

private:
    /// @brief Validates and configures the arguments.
    void _validateArguments();

    uint64_t uint_prova;
};