#pragma once

#include <hifCore.hh>
#include <cstring>
#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <sstream>
#include <regex>

#include <hif/classes/System.hh>
#include <hif/classes/Identifier.hh>
#include <hif/classes/DesignUnit.hh>
#include <hif/classes/Expression.hh>
#include <hif/classes/Port.hh>
#include <hif/classes/PortAssign.hh>
#include <hif/classes/Signal.hh>
#include <hif/classes/ValueTP.hh>
#include <hif/classes/Value.hh>
#include <hif/classes/ViewReference.hh>
#include <hif/classes/When.hh>

#include <hif/classes/BitValue.hh>
#include <hif/classes/BitvectorValue.hh>
#include <hif/classes/BoolValue.hh>
#include <hif/classes/CharValue.hh>
#include <hif/classes/EnumValue.hh>
#include <hif/classes/IntValue.hh>
#include <hif/classes/RealValue.hh>
#include <hif/classes/StringValue.hh>
#include <hif/classes/TimeValue.hh>

#include <hif/classes/ConstValue.hh>
#include <hif/classes/RecordValueAlt.hh>
#include <hif/classes/RecordValue.hh>


// Constant strings
const std::string prefixTagName = "prefix";
const std::string indexTagName = "index";
const std::string rhsTagName = "rightHandSide";
const std::string lhsTagName = "leftHandSide";
const std::string conditionTagName = "condition";
const std::string valueTagName = "value";
const std::string value1TagName = "value1";
const std::string value2TagName = "value2";
const std::string defaultTagName = "default";
const std::string delayTagName = "delay";

// <haveDependency, shortest path length>
typedef std::pair<bool, unsigned int> dependency;


// If modifyVerbose = True, set verbose
// Retun True if if verbose output is enabled
bool setVerbose(bool modifyVerbose, bool isVerbose);

// Return True if verbose output is enabled
bool isVerbose();


// Return True if they are the same object (same name and same design unit)
bool areSameObject(hif::Object *nodeA, hif::Object *nodeB);


// Given an object, visit the HIF tree backwards. If the Design unit pointer is found, return it.
hif::DesignUnit *getDesignUnit(hif::Object *obj);


// Given an object, visit the HIF tree backwards. Return True if a classId object is found.
bool isInsideClassTag(hif::Object *o, hif::ClassId classId);

// Given an object, visit the HIF tree backwards. Return the classId of the nearest classId1 or classId2 object.
hif::ClassId nearestClassTag(hif::Object *o, hif::ClassId classId1, hif::ClassId classId2);

// Given an object, visit the HIF tree backwards. If a classId object is found, return it.
hif::Object *returnClassObj(hif::Object *o, hif::ClassId classId);

// Given an object, visit the HIF tree backwards. Return True if a object with tagName is found.
bool isInsideTagName(hif::Object *o, std::string tagName);

// Given an object, visit the HIF tree backwards. Return the tagName of the nearest tagName1 or tagName2 object.
std::string nearestTagName(hif::Object *o, std::string tagName1, std::string tagName2);

// Given an object, visit the HIF tree backwards. Return the string of the nearest classId or tagName object.
std::string nearestClassOrTagName(hif::Object *o, hif::ClassId classId, std::string tagName);


// Return 'l' or 'r' if the object is inside the left hand side or the right hand side of the assign.
// If not inside an assign, return ' '
char isLeftOrRightHandSide(hif::Object *o);

// Return True if the object is inside an index ("i" in "a[i]" or in "a[j:i]")
bool isIndex(hif::Object *o);

// Return True if the object is a condition
bool isCondition(hif::Object *obj);

// Return True if the object is a value
bool isValue(hif::Object *obj);


// Given an object, return the name
std::string getObjName(hif::Object *obj);

// Return the value of the object as a string
std::string getValueAsString(hif::Object *obj);

// Return the value or the name of the object
std::string getObjString(hif::Object *obj);

// Return the classId if the object as a string (removed "Alt", "Generate" and added "Ternary")
std::string getClassIDString(hif::Object *obj);

// Return True if the object is inside the vector
bool isInsideVector(hif::Object *obj, std::vector<hif::Object *> *objVector);

// Given an object, visit the HIF tree backwards. Returns True if the object is inside a condition.
bool isPartOfCondition(hif::Object *o);


// Assigns ofstream to cout. It returns the "backup" of the current cout.
std::streambuf *setCout(std::ofstream *file);

// Assigns streambuf to cout
void setCout(std::streambuf *sb);


// Left trim
std::string ltrim(const std::string &s);

// Right trim
std::string rtrim(const std::string &s);

// Left and right trim
std::string trim(const std::string &s);
