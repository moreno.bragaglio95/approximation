#pragma once

#include "utils.hpp"

#include <algorithm>
#include <tuple>


//Control Data Flow Graph class
class Graph {

public:
    // Constructor
    Graph();

    // Print the graph
    void printGraph();

    // Return the graph's size (#arch)
    int size();

    // Add the arch "fromNode --> toNode" into the graph
    bool add(hif::Object *fromNode, hif::Object *toNode);

    // Remove the arch "fromNode --> toNode" from the graph
    bool remove(hif::Object *fromNode, hif::Object *toNode);

    // Return True if the arch "fromNode --> toNode" is inside the graph
    bool exist(hif::Object *fromNode, hif::Object *toNode);

    // Retrieve the list of all the objects related to the node
    std::multimap<hif::Object *, hif::Object *> getArchesFromNode(hif::Object *node, bool isFirst);

    // Return graph's iterator, begin()
    std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator graphBegin();

    // Return graph's iterator, end()
    std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator graphEnd();

    // Merge the graph fromGraph into the current graph
    void mergeGraph(Graph *fromGraph,
                    std::vector<std::tuple<hif::Object *, hif::Object *, hif::PortDirection>> *objBindVector);

    // Count the minimum distance (minimum number of arches) from objStart to objTarget
    const dependency minimumDistance(hif::Object *objStart, hif::Object *objTarget);


private:
    // Return the homonymous node (same name, same design unit)
    hif::Object *getHomonymousNode(hif::Object *node);

    // Recursive minimumDistance
    dependency minimumDistanceRecursive(dependency currentDeps,                     // Current distance
                                        dependency minDeps,                         // Min. distance calculated
                                        hif::Object *objCurrent,                    // Current object
                                        hif::Object *objTarget,                     // Target object
                                        std::vector<hif::Object *> currentPath);    // Current path


    // <objStart, [...objTarget..]>
    std::map<hif::Object *, std::vector<hif::Object *>> graph;

    // Graph iterator
    std::map<hif::Object *, std::vector<hif::Object *>>::const_iterator it_graph;

    // Minimum distances
    // fromObject-toObject, dependency
    std::map<hif::Object *, std::map<hif::Object *, dependency> > minDistances;
};
