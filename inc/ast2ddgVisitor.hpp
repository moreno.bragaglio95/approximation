/*
 * This class is used in order to visit the XML through the hif::GuideVisitor class and generate the concerned Data Dependencies Graph (DDG)
 * INPUT = xml file generated with verilog2ast command
 * OUTPUT = ddg
 *
 * General methodology parts:
 * 1) Every module generate a DDG but during the visiting I will catch the linking between module call and the considered variables.
 *    At the end of all module visits a renaming will be done.
 *      example:
 *          in module_main there is a statement:            andModule(A,B,C);
 *          in module_and(X,Y,Z) there is a statement:      X = Y & Z;
 *          then the ddg for module_and will be generated:
 *                  z <-- X --> Y
 *          and at the end of all visiting a renaming will e performed (X takes A, Y takes B, Z takes C)
 *                  C <-- A --> B
 * */

#pragma once

#include <hif/GuideVisitor.hh>
#include "Graph.hpp"
#include "utilsDbm.hpp"


class ast2ddgVisitor : public hif::GuideVisitor {

public:
    // Constructors
    ast2ddgVisitor();

    ast2ddgVisitor(std::map<hif::DesignUnit *, DBM> *dbmMap);

    // Visitors
    int visitLibraryDef(hif::LibraryDef &o) override;

    int visitDesignUnit(hif::DesignUnit &o) override;

    int visitPort(hif::Port &o) override;

    int visitSignal(hif::Signal &o) override;

    int visitValueTP(hif::ValueTP &o) override;

    int visitIdentifier(hif::Identifier &o) override;

    int AfterVisit(hif::Object &o) override;

    // Print graph arches
    void printGraph();


private:
    // Check the position (LHS, RHS) and then add the identifier to the LeftHandSide set or the RightHandSide set
    void addIdToHs(hif::Identifier *o);

    // Add arches to the graph (RHS set objets --> LHS set object)
    void addHsToGraph();


    // Last visited objects
    hif::DesignUnit *lastVisitedDU = nullptr;
    hif::Object *lastVisitedPrefix = nullptr;
    hif::Object *lastVisitedPort = nullptr;
    hif::Object *lastVisitedSignal = nullptr;
    hif::Object *lastVisitedValueTP = nullptr;

    // Left hand side
    std::set<hif::Object *> setLHS;
    std::set<hif::Object *>::iterator it_setLHS;

    // Right hand side
    std::set<hif::Object *> setRHS;
    std::set<hif::Object *>::iterator it_setRHS;

    // DBM map (Data Between Modules)
    std::map<hif::DesignUnit *, DBM> *dbmMap;

    bool verbose;
};
