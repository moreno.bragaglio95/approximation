#pragma once

#include <hifCore.hh>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <string>
#include <map>


// Assertion file parser class
class AssertionDepthsFileParser {

public:
    // Constructor
    AssertionDepthsFileParser(std::string projDirPath,
                              std::vector<std::map<std::string, size_t>> *assertionsDepths,
                              std::vector<std::pair<size_t, size_t>> *assertionContingency);

    // Fill assertion depth vector
    void setAssertionDepths();

    // Fill assertion contingency vector
    void setAssertionContingency();


private:
    // Assertion depth vector:
    // for each assertion save identifier-depth pair
    // map<identifier string, identifier depth>
    std::vector<std::map<std::string, size_t>> *assertionDepths;

    // Assertion contingency vector:
    // for each assertion save all and TrueTrue instances
    // vector<pair{all instances, True antecedent-True consequent instances}>
    std::vector<std::pair<size_t, size_t>> *assertionContingency;

    // Project directory path
    std::string projDirPath;

    // Assertion depth filename
    std::string assertionDepthsFileName = "default_vars.txt";

    // Assertion contingency filename
    std::string assertionContingencyFileName = "default_cont.txt";
};
