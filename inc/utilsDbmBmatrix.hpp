#pragma once

#include <utilsDbm.hpp>
#include <Bmatrix.hpp>

#include <dirent.h>
#include <fstream>
#include <unordered_map>


// After order mode selection, it creates a B matrix for each Design unit in duVector
void getBmatrixs(std::map<hif::DesignUnit *, DBM> *dbmMap,
                 std::vector<hif::DesignUnit *> duVector,
                 hif::System *system,
                 std::unordered_map<std::string, std::unordered_map<size_t, size_t>> *statementCoverageMap,
                 std::unordered_map<std::string, std::unordered_map<size_t, std::tuple<size_t, size_t, size_t, hif::ClassId> >> *branchCoverageMap,
                 std::vector<std::map<std::string, size_t>> *assertionDepths,
                 std::vector<std::pair<size_t, size_t>> *assertionContingency,
                 std::string testDirPath,
                 std::string projDirPath);

// Return the selected order mode
int selectOrderMode();

// Return the path of the new test directory
std::string getTestPath(std::string argv1);

// Add the 3-digit number to the string
std::string add3DigitNumberToString(std::string str, unsigned int n);

// Check Hif.xml file path. Hif.xml file should be inside .../approximation/test/*myProjName*/ directory
void checkHifXmlPath(std::string pathHifXmlFile);

// Remove filename from path (if path doesn't have '/', return "./")
std::string removeFilenameFromPath(std::string path);

// From test directory path to project directory path
std::string getProjPathFromTestPath(std::string path);
