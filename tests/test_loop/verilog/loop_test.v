module for_loop (i_Clock, out1, out2);
    input i_Clock;

    integer ii=0;
    reg [7:0] r_Data[15:0]; // Create reg 8 bit wide by 16 words deep.

    initial
    begin

        // Sets first value of r_Data to allow while loop to execute
        // (Allows it to be true on the first iteration)
        r_Data[ii] = ii*ii;

        while (r_Data[ii] < 100)
        begin
            ii = ii + 1;
            r_Data[ii] = (ii % 2 == 0)? ii*ii : ii;
            #10;
        end
    end



    output integer out1;
    output integer out2;
    integer a, b;
    integer i1, i2, j1, j2;


    initial
    begin
        // Nested for
        for (i1=0; i1<2; i1=i1+1) begin
            for (j1=0; j1<8; j1=j1+1) begin
                a = a + 1'b1;
            end
        end

        out1 = a;
    end

    initial
    begin
        // Not nested for
        for (i2=0; i2<2; i2=i2+1) begin
            b = b + 1'b1;
        end

        for (j2=0; j2<8; j2=j2+1) begin
            b = b - 1'b1;
        end

        out2 = b;
    end

endmodule
