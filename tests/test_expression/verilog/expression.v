module  expression(in, out);
	input in;
	output reg out;

	integer a = 4;
	integer b = 5;
	integer c = 6;
	integer d = 7;
	integer e = 8;
	reg boolean = 1'b0;
	reg boolean2 = 1'b0;
	reg[1:0] due;
	reg[2:0] tre;

	initial begin
		a = b + c;
		b = c - (d + e);
		c = (d + e) - b;
		e = (boolean) ? a : c;
		e = (boolean && boolean2) ? b : d;

		boolean = ~( boolean2);
	end

	always @(a) begin
		if(a>9) begin
			due = {boolean, boolean2};
			{boolean, boolean2} = due;

			tre = {boolean2, boolean, boolean2};
			{due, boolean2} = tre;
		end

		out = boolean;
	end

endmodule
