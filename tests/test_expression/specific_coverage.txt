Coverage Report by instance with details

=================================================================================
=== Instance: /expression
=== Design Unit: work.expression
=================================================================================
Branch Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Branches                         6         3         3    50.00%

================================Branch Details================================

Branch Coverage for instance /expression

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/test_expression/expression.v
------------------------------------IF Branch------------------------------------
    19                                         1     Count coming in to IF
    19              1                    ***0***     
    19              2                          1     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    20                                         1     Count coming in to IF
    20              1                    ***0***     
    20              2                          1     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    26                                         1     Count coming in to IF
    26              1                          1     
                                         ***0***     All False Count
Branch totals: 1 hit of 2 branches = 50.00%


Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                      19        19         0   100.00%

================================Statement Details================================

Statement Coverage for instance /expression --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/test_expression/expression.v
    5               1                          1     
    6               1                          1     
    7               1                          1     
    8               1                          1     
    9               1                          1     
    10              1                          1     
    11              1                          1     
    16              1                          1     
    17              1                          1     
    18              1                          1     
    19              1                          1     
    20              1                          1     
    22              1                          1     
    25              1                          1     
    27              1                          1     
    28              1                          1     
    30              1                          1     
    31              1                          1     
    34              1                          1     


Total Coverage By Instance (filtered view): 75.00%

