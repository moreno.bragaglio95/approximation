module example(in, out);
	input in;
	output out;
	reg a = 1'b1;

	assign out = in ^ a;
endmodule