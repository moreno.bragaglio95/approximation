###### Module: RisingEdge_DFlipFlop_AsyncResetHigh ######


[RisingEdge_DFlipFlop_AsyncResetHigh] Outputs list:
	out0. 	Q


	out0      Method result  Hit            Design Unit                           Hif type       Line number    Statements     
	1/0       1              0              RisingEdge_DFlipFlop_AsyncResetHigh   Assign         15             Q <= 1'b0;
	1/0       1              0              RisingEdge_DFlipFlop_AsyncResetHigh   Assign         17             Q <= D;
	1/1       1              0              RisingEdge_DFlipFlop_AsyncResetHigh   If             14             if(async_reset==1'b1)

