Coverage Report by instance with details

=================================================================================
=== Instance: /testbench/gcd0/dp/d32/sub/a0
=== Design Unit: work.adder64
=================================================================================
Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                       5         5         0   100.00%

================================Statement Details================================

Statement Coverage for instance /testbench/gcd0/dp/d32/sub/a0 --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/Adder64.v
    10              1                          1     
    11              1                          6     
    12              1                        695     
    13              1                        695     
    14              1                         23     

=================================================================================
=== Instance: /testbench/gcd0/dp/d32/sub
=== Design Unit: work.ALU64
=================================================================================
Branch Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Branches                        24         3        21    12.50%

================================Branch Details================================

Branch Coverage for instance /testbench/gcd0/dp/d32/sub

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/ALU.v
------------------------------------IF Branch------------------------------------
    27                                        13     Count coming in to IF
    27              1                    ***0***     
    27              2                         13     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    30                                        17     Count coming in to IF
    30              1                    ***0***     
    30              2                         17     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------CASE Branch------------------------------------
    35                                        72     Count coming in to CASE
    36              1                    ***0***     
    37              1                    ***0***     
    38              1                    ***0***     
    39              1                    ***0***     
    40              1                    ***0***     
    46              1                         72     
    47              1                    ***0***     
    53              1                    ***0***     
    54              1                    ***0***     
    55              1                    ***0***     
    56              1                    ***0***     
    57              1                    ***0***     
    58              1                    ***0***     
    59              1                    ***0***     
    60              1                    ***0***     
    61              1                    ***0***     
Branch totals: 1 hit of 16 branches = 6.25%

------------------------------------IF Branch------------------------------------
    42                                   ***0***     Count coming in to IF
    42              1                    ***0***     
                                         ***0***     All False Count
Branch totals: 0 hits of 2 branches = 0.00%

------------------------------------IF Branch------------------------------------
    49                                   ***0***     Count coming in to IF
    49              1                    ***0***     
                                         ***0***     All False Count
Branch totals: 0 hits of 2 branches = 0.00%


Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                      30        13        17    43.33%

================================Statement Details================================

Statement Coverage for instance /testbench/gcd0/dp/d32/sub --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/ALU.v
    15              1                          1     
    16              1                          1     
    18              1                          1     
    20              1                         10     
    23              1                         72     
    24              1                          1     
    25              1                          8     
    27              1                         13     
    28              1                          3     
    29              1                         69     
    30              1                         17     
    34              1                         72     
    36              1                    ***0***     
    37              1                    ***0***     
    38              1                    ***0***     
    39              1                    ***0***     
    41              1                    ***0***     
    43              1                    ***0***     
    46              1                         72     
    48              1                    ***0***     
    50              1                    ***0***     
    53              1                    ***0***     
    54              1                    ***0***     
    55              1                    ***0***     
    56              1                    ***0***     
    57              1                    ***0***     
    58              1                    ***0***     
    59              1                    ***0***     
    60              1                    ***0***     
    61              1                    ***0***     

=================================================================================
=== Instance: /testbench/gcd0/dp/d32
=== Design Unit: work.divider32
=================================================================================
Branch Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Branches                        11         9         2    81.81%

================================Branch Details================================

Branch Coverage for instance /testbench/gcd0/dp/d32

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/divider32.v
------------------------------------IF Branch------------------------------------
    20                                        88     Count coming in to IF
    20              1                          4     
    24              1                         84     
                                         ***0***     All False Count
Branch totals: 2 hits of 3 branches = 66.66%

------------------------------------IF Branch------------------------------------
    33                                        78     Count coming in to IF
    33              1                          6     
                                              72     All False Count
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    42                                        78     Count coming in to IF
    42              1                         10     
                                              68     All False Count
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    46                                        78     Count coming in to IF
    46              1                          2     
                                              76     All False Count
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    48                                         2     Count coming in to IF
    48              1                          2     
                                         ***0***     All False Count
Branch totals: 1 hit of 2 branches = 50.00%


Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                      17        17         0   100.00%

================================Statement Details================================

Statement Coverage for instance /testbench/gcd0/dp/d32 --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/divider32.v
    19              1                         88     
    21              1                          4     
    22              1                          4     
    25              1                         84     
    32              1                         78     
    34              1                          6     
    35              1                          6     
    36              1                          6     
    37              1                          6     
    39              1                         78     
    40              1                         78     
    41              1                         78     
    43              1                         10     
    44              1                         10     
    47              1                          2     
    49              1                          2     
    50              1                          2     

=================================================================================
=== Instance: /testbench/gcd0/mf
=== Design Unit: work.masterFSM
=================================================================================
Branch Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Branches                         8         8         0   100.00%

================================Branch Details================================

Branch Coverage for instance /testbench/gcd0/mf

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/masterFSM.v
------------------------------------IF Branch------------------------------------
    11                                        87     Count coming in to IF
    11              1                          5     
    12              1                         82     
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------CASE Branch------------------------------------
    16                                        10     Count coming in to CASE
    17              1                          4     
    26              1                          6     
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    21                                         4     Count coming in to IF
    21              1                          1     
                                               3     All False Count
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    30                                         6     Count coming in to IF
    30              1                          1     
                                               5     All False Count
Branch totals: 2 hits of 2 branches = 100.00%


Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                      14        14         0   100.00%

================================Statement Details================================

Statement Coverage for instance /testbench/gcd0/mf --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/masterFSM.v
    10              1                         87     
    11              1                          5     
    12              1                         82     
    15              1                         10     
    18              1                          4     
    19              1                          4     
    20              1                          4     
    22              1                          1     
    23              1                          1     
    27              1                          6     
    28              1                          6     
    29              1                          6     
    31              1                          1     
    32              1                          1     

=================================================================================
=== Instance: /testbench/gcd0/gf
=== Design Unit: work.GCDFSM
=================================================================================
Branch Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Branches                        32        23         9    71.87%

================================Branch Details================================

Branch Coverage for instance /testbench/gcd0/gf

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/GCDFSM.v
------------------------------------IF Branch------------------------------------
    26                                        87     Count coming in to IF
    26              1                          5     
    27              1                         82     
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------CASE Branch------------------------------------
    31                                        35     Count coming in to CASE
    32              1                          7     
    39              1                          2     
    51              1                          4     
    60              1                          3     
    65              1                          8     
    75              1                          6     
    82              1                          2     
    87              1                    ***0***     
    93              1                          2     
    98              1                    ***0***     
    102             1                    ***0***     
    107             1                          1     
Branch totals: 9 hits of 12 branches = 75.00%

------------------------------------IF Branch------------------------------------
    33                                         7     Count coming in to IF
    33              1                          1     
    33              2                          6     
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    43                                         2     Count coming in to IF
    43              1                    ***0***     
    43              2                          2     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    44                                         2     Count coming in to IF
    44              1                          2     
    44              2                    ***0***     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    45                                         2     Count coming in to IF
    45              1                    ***0***     
                                               2     All False Count
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    56                                         4     Count coming in to IF
    56              1                          2     
                                               2     All False Count
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    68                                         8     Count coming in to IF
    68              1                          2     
    68              2                          6     
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    69                                         8     Count coming in to IF
    69              1                          2     
                                               6     All False Count
Branch totals: 2 hits of 2 branches = 100.00%

------------------------------------IF Branch------------------------------------
    84                                         2     Count coming in to IF
    84              1                    ***0***     
    84              2                          2     
Branch totals: 1 hit of 2 branches = 50.00%

------------------------------------IF Branch------------------------------------
    100                                  ***0***     Count coming in to IF
    100             1                    ***0***     
    100             2                    ***0***     
Branch totals: 0 hits of 2 branches = 0.00%


Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                      50        39        11    78.00%

================================Statement Details================================

Statement Coverage for instance /testbench/gcd0/gf --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/GCDFSM.v
    25              1                         87     
    26              1                          5     
    27              1                         82     
    30              1                         35     
    33              1                          7     
    34              1                          7     
    35              1                          7     
    36              1                          7     
    37              1                          7     
    40              1                          2     
    41              1                          2     
    42              1                          2     
    43              1                          2     
    44              1                          2     
    47              1                    ***0***     
    48              1                    ***0***     
    52              1                          4     
    54              1                          4     
    55              1                          4     
    57              1                          2     
    61              1                          3     
    62              1                          3     
    63              1                          3     
    66              1                          8     
    67              1                          8     
    68              1                          8     
    70              1                          2     
    71              1                          2     
    72              1                          2     
    76              1                          6     
    77              1                          6     
    78              1                          6     
    79              1                          6     
    80              1                          6     
    83              1                          2     
    84              1                          2     
    85              1                          2     
    88              1                    ***0***     
    89              1                    ***0***     
    90              1                    ***0***     
    91              1                    ***0***     
    94              1                          2     
    95              1                          2     
    96              1                          2     
    99              1                    ***0***     
    100             1                    ***0***     
    103             1                    ***0***     
    104             1                    ***0***     
    105             1                    ***0***     
    108             1                          1     

=================================================================================
=== Instance: /testbench
=== Design Unit: work.testbench
=================================================================================
Statement Coverage:
    Enabled Coverage              Bins      Hits    Misses  Coverage
    ----------------              ----      ----    ------  --------
    Statements                      31        31         0   100.00%

================================Statement Details================================

Statement Coverage for instance /testbench --

    Line         Item                      Count     Source 
    ----         ----                      -----     ------ 
  File /home/samu/repository/hif/approximation/test/GCD/testbench.v
    14              1                        171     
    14              2                        170     
    17              1                          1     
    18              1                          1     
    19              1                          1     
    20              1                          1     
    21              1                          1     
    21              2                          1     
    22              1                          1     
    22              2                          1     
    23              1                          1     
    28              1                          1     
    29              1                          1     
    30              1                          1     
    31              1                          1     
    32              1                          1     
    33              1                          1     
    34              1                          1     
    35              1                          1     
    36              1                          1     
    37              1                          1     
    38              1                          1     
    39              1                          1     
    40              1                          1     
    41              1                          1     
    42              1                          1     
    43              1                          1     
    44              1                          1     
    48              1                          1     
    50              1                          1     
    51              1                          1     


Total Coverage By Instance (filtered view): 69.14%

