module GCD(clk, rst, start, A, B, done, result, ERROR);
    input clk, rst, start;
    input [31:0] A, B;
    output done, ERROR;
    output [31:0] result;

    wire startGCD, doneGCD, startDiv, doneDiv;
    wire [31:0] dividend, divisor, quotient, remainder;
    wire [3:0] stateGCD;

    //assign done = doneGCD;

    Datapath dp(clk, startDiv, doneDiv, dividend, divisor, quotient, remainder);
    masterFSM mf(clk, rst, start, doneGCD, startGCD, done);
    GCDFSM gf(clk, rst, startGCD, doneGCD, startDiv, doneDiv, A, B, dividend, divisor, quotient, remainder, result, ERROR);
endmodule
