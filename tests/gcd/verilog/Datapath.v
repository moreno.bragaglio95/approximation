module Datapath(clk, startDiv, doneDiv, dividend, divisor, quotient, remainder);
    input clk, startDiv;
    input [31:0] dividend, divisor;
    output doneDiv;
    output [31:0] quotient, remainder;

    divider32 d32(clk, startDiv, dividend, divisor, quotient, remainder, doneDiv);
endmodule
