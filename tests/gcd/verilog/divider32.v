`define SUB 4'b0101
module divider32(clk, start, dividend, divisor, quotient, remainder, done);
    input clk, start;
    input [31:0] dividend, divisor;
    output done;
    output [31:0] quotient, remainder;

    reg [31:0] quotient, remainder;
    reg done;
    //expand dividend and divisor,
    reg [63:0] long_dividend;
    reg [63:0] long_divisor;
    reg [4:0] state;
    reg inner_start;
    wire [63:0] differ;
    wire dontcare;

    // finite state machine
    always @(posedge clk or posedge start) begin
        if (start) begin
            state = 0;
            inner_start = 1;
        end
        else if (clk) begin
            state = state + 1;
        end
    end

    ALU64 sub(long_dividend, long_divisor, 1'b0, `SUB, differ, dontcare, dontcare, dontcare, dontcare);

    // Datapath
    always @(*) begin
        if (state == 0) begin
            long_dividend = {{32{1'b0}}, dividend};
            long_divisor  = {divisor,  {32{1'b0}}};
            quotient = 0;
            remainder = 0;
        end
        long_divisor = long_divisor >> 1;
        quotient = quotient << 1;
        done = 0;
        if (long_dividend >= long_divisor) begin
            long_dividend = long_dividend - long_divisor;
            quotient = quotient + 1;
        end
        if (state == 31) begin
            remainder = long_dividend[31:0];
            if (inner_start) begin
                inner_start = 0;
                done = 1;
            end
        end
    end
endmodule
