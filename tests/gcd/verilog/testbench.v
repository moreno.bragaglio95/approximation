module testbench();
    parameter cyc = 10; // clock cycle
    parameter delay = cyc/2;
    parameter delta = cyc/5;

    wire done, ERROR;
    wire [31:0] result;
    reg clk, rst, start;
    reg [31:0] AA, BB;

    GCD gcd0(clk, rst, start, AA, BB, done, result, ERROR);

    // clock
    always #(cyc/2) clk = ~clk;

    initial begin
        rst = 1'b0;
        clk = 1'b1;
        start = 1'b0;
        #(cyc);
        #(delay) rst = 1;
        #(cyc*4) rst = 0;
        #(delay);
		
	
		
		// --------- TEST_3
		$display(" \n\n-------- TEST 3 -------- ");
        #(delta);
        AA=32'd144; 
        BB=32'd180; 
        start = 1'b1;
        $display("AA = %d, BB = %d", AA, BB);
        #(cyc);
        start = 1'b0;
        $display("start = 1'b0");
        #(cyc)
        $display("waiting done");
        @(posedge done);
        #(delay);
        #(delay);
        $display("\tGCD = %d, ERROR = %d", result, ERROR);
        #(delay);
        #(cyc);

		
		
		$display(" \n\n -------- TEST ENDED --------");

        #(2*cyc);
        $finish;
    end
endmodule
