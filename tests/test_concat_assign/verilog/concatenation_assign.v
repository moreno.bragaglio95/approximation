module concatenation_assign(out);
	output out;

	reg a,b,c,d,e;
	reg[4:0] s = 5'b10100;
	reg[4:0] t;

	initial begin
	    {a,b,c,d,e} = s;
	    t = {a,b,c,d,e};
	end

	assign out = t[0];
endmodule
