cmake_minimum_required(VERSION 2.8.11)

project(approximation)

include_directories(
        inc
)

add_executable(
        ${PROJECT_NAME}
        src/main.cpp
        src/parseLine.cpp
        src/ast2ddgVisitor.cpp
        src/ast2cfgVisitor.cpp
        src/ast2moduleLinking.cpp
        src/ast2Bmatrix.cpp
        src/CoverageFileParser.cpp
        src/AssertionFilesParser.cpp
        src/Bmatrix.cpp
        src/Graph.cpp
        src/utils.cpp
        src/utilsDbm.cpp
        src/utilsDbmBmatrix.cpp)

target_link_libraries(
        ${PROJECT_NAME}
        hif
)